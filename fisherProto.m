numFeatures = 4096;
dimension = 1;
gmmInputData = rand(dimension, numFeatures);

numClusters = 30;
numRepetitions = 1;
covarianceBound = 10e-6;
[means, covariances, priors] = vl_gmm(gmmInputData, numClusters, ...
    'NumRepetitions', numRepetitions, ...
    'CovarianceBound', covarianceBound);

%numDataToEncode = 1000;
%dataToBeEncoded = rand(dimension, numDataToEncode);

%enc = vl_fisher(dataToBeEncoded, means, covariances, priors);
enc = vl_fisher(gmmInputData, means, covariances, priors);