% D1 dataset, image cropping, storing severity, and names into an object
% file
if exist('data/uncroppedimagesD1.mat','file')
    fprintf('loading saved uncroppedimagesD1.mat. Preprocessing...\n');
    imageUncroppedData = load('data/uncroppedimagesD1.mat');       
else        
    fprintf('uncroppedimagesD1.mat does not exist! Preprocessing...\n');
    imagePath = '../KLD_TF/Images';
    imageFiles = dir(fullfile(imagePath, '*.bmp'));
    imageUncroppedData={};
    imageUncroppedData.severity= zeros(939,1);
    imageUncroppedData.imagemean = [];
    numberOfImages = size(imageFiles,1);
    numberOfImages = 6;
    % 4 fold cross validation set preparation
    % First Set
    train1Str = fileread('../Train1.txt');   %read entire file into string
    train1parts = strtrim(regexp( train1Str, '(\r|\n)+', 'split'));  %split by each line    
    test1Str = fileread('../Test1.txt');   %read entire file into string
    test1parts = strtrim(regexp( test1Str, '(\r|\n)+', 'split'));  %split by each line

    % Second Set
    train2Str = fileread('../Train2.txt');   %read entire file into string
    train2parts = strtrim(regexp( train2Str, '(\r|\n)+', 'split'));  %split by each line    
    test2Str = fileread('../Test2.txt');   %read entire file into string
    test2parts = strtrim(regexp( test2Str, '(\r|\n)+', 'split'));  %split by each line

    % Third Set
    train3Str = fileread('../Train3.txt');   %read entire file into string
    train3parts = strtrim(regexp( train3Str, '(\r|\n)+', 'split'));  %split by each line    
    test3Str = fileread('../Test3.txt');   %read entire file into string
    test3parts = strtrim(regexp( test3Str, '(\r|\n)+', 'split'));  %split by each line

    % Fourth Set
    train4Str = fileread('../Train4.txt');   %read entire file into string
    train4parts = strtrim(regexp( train4Str, '(\r|\n)+', 'split'));  %split by each line    
    test4Str = fileread('../Test4.txt');   %read entire file into string
    test4parts = strtrim(regexp( test4Str, '(\r|\n)+', 'split'));  %split by each line        
    fclose('all');
    %sumOfTrainMeans = zeros(1,4);
    %sumOfTestMeans = zeros(1,4);
    for j = 1: numberOfImages
        imagename = imageFiles(j).name;
        severity = getSeverityFromFilename(imagename);
        image = imread(fullfile(imagePath, imagename));        
        image = im2single(image);
        [height, width, m] = size(image);                 
        sets = getSetLabelForFileNameFor4FoldCrossValidation(train1parts, test1parts, ...
                                                 train2parts, test2parts, ...
                                                 train3parts, test3parts, ...
                                                 train4parts, test4parts, ...
                                                 imagename);
        currentImageMean = mean(mean(image));
        imageUncroppedData.imagemean(j) = currentImageMean;
        imageUncroppedData.names{j} = imagename;
        imageUncroppedData.severity(j) = severity;                                                 
        imageUncroppedData.setlabels{j} = sets;            
    end
    fprintf('saving uncroppedimagesD1.mat. Preprocessing...\n');
    %save('data/uncroppedimagesD1.mat', '-struct', 'imageUncroppedData') ;
end
allImageMean = mean(imageUncroppedData.imagemean) ;
fprintf('Loading VGG-M...\n');
vggnet = load('..\imagenet-vgg-m.mat');
newNet = vggnet;
for i = 1: numel(vggnet.layers)
    if strcmp(vggnet.layers{1,i}.type,'conv')
        newNet.layers{1,i} = rmfield(vggnet.layers{1,i},'weights');
        newNet.layers{1,i}.filters = vggnet.layers{1,i}.weights{1,1};
        newNet.layers{1,i}.biases = vggnet.layers{1,i}.weights{1,2};
    end
end
fprintf('Loading VGG-M completed! \n');
if ~exist('data/cnnfeatures.mat','file')
    for i = 1:numberOfImages
        imagename = imageUncroppedData.names{j};
        image = imread(fullfile(imagePath, imagename));     
        image = im2single(image);        
        fprintf('Subtracting Mean for cropped image\n');
        image = image - allImageMean;
        [r c m] = size(image);       
        if m < 3
            image = cat(3, image, image, image);
        end
        netres = vl_simplenn(newNet, image);
        imageFeatures = netres(20).x;
        % fetch CNN features 
        fprintf(strcat('Fetching CNN features for image',num2str(i),'\n'));
        cnnImageFeatures{i} = reshape(imageFeatures,size(imageFeatures,1)*size(imageFeatures,2),size(imageFeatures,3));
    end
else
    fprintf(strcat('Loading CNN features for images from file','\n'));
    cnnImageFeatures = load('data/cnnfeatures.mat');
    cnnImageFeatures = cnnImageFeatures.cnnImageFeatures;
end

numClusters = 45;
numRepetitions = 10; 
covarianceBound = 10e-3;
fisherNormalization = 'Improved';
result = {};

trainingsetImages={};
trainingsetImages.set={};        
testingsetImages={};
testingsetImages.set={};
fprintf('Performing 4 fold cross validations\n');    
% Following 4 fold cross validations
for fourfoldcounter = 1:1
    fprintf(strcat('CrossValidation Test ',num2str(fourfoldcounter), '\n'));    
    trainingsetImages.set{fourfoldcounter}.severity = [];
    testingsetImages.set{fourfoldcounter}.severity = [];
    allCNNFeaturesForGMMRun = [];
    cnnImagesForTraining = {};
    trainCounter = 0;   
    for i = 1:numberOfImages
        setarray = imageUncroppedData.setlabels{i};        
        imageType = setarray(fourfoldcounter);
        if imageType == 1
            trainCounter = trainCounter + 1;
            cnnImagesForTraining{trainCounter} = cnnImageFeatures{i};           
            trainingsetImages.set{fourfoldcounter}.severity = [trainingsetImages.set{fourfoldcounter}.severity; imageUncroppedData.severity(i)];
        else            
            testingsetImages.set{fourfoldcounter}.severity = [testingsetImages.set{fourfoldcounter}.severity; imageUncroppedData.severity(i)];            
        end        
    end   
    fprintf(strcat('CrossValidation Test: Vertical concatenation of features!','\n'));    
    allCNNFeaturesForGMMRun = vertcat(cnnImagesForTraining{1:trainCounter});
    fprintf(strcat('Computing GMM for training images belonging to training set run = ',num2str(fourfoldcounter),'\n'));
    [means, covariances, priors] = vl_gmm(allCNNFeaturesForGMMRun', ...
                                          numClusters, ...
                                        'NumRepetitions', numRepetitions, ...
                                        'CovarianceBound', covarianceBound);
    normalization = 'Improved';
    rowStart = 1;    
    trainCounter = 1;
    testCounter = 1;
    fisherFeatures = [];
    for i = 1:numberOfImages               
        fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),'. Computing Fisher Vectors for image ',num2str(i),'! \n'));
        rowsizeOfImageFeatures = size(cnnImageFeatures{i},1);
        fisherFeatures(i,:) = vl_fisher(cnnImageFeatures{i}, ...
                                     means(rowStart:rowStart + rowsizeOfImageFeatures - 1,:), ...
                                     covariances(rowStart:rowStart + rowsizeOfImageFeatures - 1,:), ...
                                     priors, ...
                                     normalization);
        rowStart = rowStart + rowsizeOfImageFeatures; 
        imageTypeArr = imageUncroppedData.setlabels{i};                                  
        imageType = imageTypeArr(fourfoldcounter);
        if (imageType == 1)
            trainingsetImages.set{fourfoldcounter}.fisherFeatures(trainCounter,:) = fisherFeatures(i,:);                                 
            trainCounter = trainCounter + 1;
        else            
            testingsetImages.set{fourfoldcounter}.fisherFeatures(testCounter,:) = fisherFeatures(i,:);                                 
            testCounter = testCounter + 1;
        end        
    end
    % Train SVM
    fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),' Training SVM','\n'));
    svmTrainingModel = svmtrain(trainingsetImages.set{fourfoldcounter}.severity, trainingsetImages.set{fourfoldcounter}.fisherFeatures);
    fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),' Predicting for training set','\n'));
    [predicted_labelTRAIN, accuracyTRAIN, prob_estimatesTRAIN] = svmpredict(trainingsetImages.set{fourfoldcounter}.severity, sparse(trainingsetImages.set{fourfoldcounter}.fisherFeatures), svmTrainingModel);        
    [predicted_labelTEST, accuracyTEST, prob_estimatesTEST] = svmpredict(testingsetImages.set{fourfoldcounter}.severity, sparse(testingsetImages.set{fourfoldcounter}.fisherFeatures), svmTrainingModel);                   
    %result.set(fourfoldcounter).optimization(optcounter).numcluster = numClusters;
    %result.set(fourfoldcounter).optimization(optcounter).numiterations = numRepetitions;
    %result.set(fourfoldcounter).optimization(optcounter).trainingaccuracy = [predicted_labelTRAIN, accuracyTRAIN, prob_estimatesTRAIN];
    %result.set(fourfoldcounter).optimization(optcounter).testingaccuracy = [predicted_labelTEST, accuracyTEST, prob_estimatesTEST];
end

