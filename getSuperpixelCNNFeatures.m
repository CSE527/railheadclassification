function [ features ] = getSuperpixelCNNFeatures( dataRoot, setId, ...
    net, images, imageMean, regionSize, regularizer )
%GETSUPERPIXELCNNFeatures Returns the CNN features of the provided
% images.
% The result is a structure that contains a matrix representing
% all the computed CNN features (as column vectors) and a set of 
% indexes into the feature set, one entry for each image.

    filename = strcat('sp_', setId, '.mat');
    
    datafilePath = fullfile(dataRoot, filename);
    
    if (exist(datafilePath, 'file'))
        features = load(datafilePath);
    else       
        fprintf('Computing Superpixel-CNN features (%d, %f)...\n', regionSize, regularizer);
        
        allCnnFeaturesCreated = 0;
        numImages = length(images);
        
        imageCnnFeaturesIndexes = cell(numImages, 1);
        for i = 1:numImages
            fprintf('Processing image %d of %d...\n', i, numImages);
            
            image = getCNNCompatImage(images{i}.data);            
            
            image = image - imageMean;
        
            cnnFeatures = extractCNNSuperPixel(net, image, regionSize, regularizer);
        
            if (allCnnFeaturesCreated == 0)
               allCnnFeatures = cnnFeatures; 
               allCnnFeaturesCreated = 1;

               indexes = {};
               indexes.startIx = 1;
               indexes.endIx = indexes.startIx + size(cnnFeatures, 2) - 1;           

               imageCnnFeaturesIndexes{i} = indexes;
            else
               indexes = {};
               indexes.startIx = size(allCnnFeatures, 2) + 1;
               indexes.endIx = indexes.startIx + size(cnnFeatures, 2) - 1;
               imageCnnFeaturesIndexes{i} = indexes;

               allCnnFeatures = cat(2, allCnnFeatures, cnnFeatures);           
            end        
        end
        
        features = {};
        features.featureIndexes = imageCnnFeaturesIndexes;
        features.features = allCnnFeatures;
        
        save(datafilePath, '-struct', 'features', '-v7.3');
    end
end

