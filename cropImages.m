% Function to crop and resize image files
% Input: 
% inPath: path to folder containing images.
% outPath: path to cropped dataset.
% imageSize: size of cropped image
function cropImages(inPath, outPath, outSize, fileExt)
    if ~exist(outPath,'dir')
        cmd = sprintf('mkdir %s', outPath);
        system(cmd);
    end
    
    imageFiles = dir(fullfile(inPath, fileExt));
    for j = 1: length(imageFiles)
    if imageFiles(j).name(1) == '.'
        continue
    else
        im = imread(fullfile(inPath, imageFiles(j).name));
        im2 = cropImage(im, outSize, outSize);
        if exist(fullfile(outPath,imageFiles(j).name),'file')
            cmd = sprintf('rm %s', fullfile(outPath,imageFiles(j).name));
            system(cmd);
        end
        imwrite(im2, fullfile(outPath,imageFiles(j).name));  
    end
end

