function [sumoftrainmeans sumoftestmeans] = sumOfTrainingAndTestingImageMeansFor4FoldCrossValidation(sumoftrainmeans, sumoftestmeans, sets, imageMean)
    for i = 1:size(sets, 2)
        if sets(i) == 1
            sumoftrainmeans(i) = sumoftrainmeans(i) + imageMean;
        else
            sumoftestmeans(i) = sumoftestmeans(i) + imageMean;
        end
    end    
end