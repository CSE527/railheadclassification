function [ name ] = getNameForFloat( value )
%GETNAMEFORFLOAT Returns a filename-compatible string for the value.

    name = sprintf('%f', value);

    if (value < 1)
       name = strrep(name, '-', 'n');
    end
    
    name = strrep(name, '.', 'd');

end

