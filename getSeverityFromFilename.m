function [ severity ] = getSeverityFromFilename( filename )
%GETSEVERITYFROMFILENAME Parses the filename and extracts the severity
%encoded within it.
% This assumes that severity is in the first position of
% <first>_<second>_<third>.<fileextension>
% Returns -1 if the severity cannot be parsed (i.e. if filename is
% malformed).

underscoreIx = find(filename == '_', 1);

if isempty(underscoreIx)
   severity = -1;
   return;
end

[severity, status] = str2num(filename(1:underscoreIx-1));

if (status == 0)
    severity = -1;
end


end

