
fprintf('Performing 4-fold cross validation on D3 using whole image.\n');

netType = 'vggm';
clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;
svmCost = 2;
kernelType = 0; % Linear
sampleRatePercent = 100;

fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');

fprintf('Loading images...\n');
imdb = getD3DatasetByFilename('../data');

% Get the features for the entire dataset
featureDb = getWholeImageCNNFeaturesByFilename('../Data', '../Data/D3', ...
    datasetName, netType, imdb, imdb.imageMean, net);

results = {};
results.results = cell(1, 4);

for setIx=1:4
    
    if (sampleRatePercent < 100)
        dataName = sprintf('4cross_wi_%s_D3_s%d', netType, sampleRatePercent);
    else
        dataName = sprintf('4cross_wi_%s_D3', netType); 
    end
    
    [ predicted_labels_train, accuracy_train, prob_estimates_train, ...
      predicted_labels_test, accuracy_test, prob_estimates_test, confusionResults ] = ...
      trainAndTest('../Data', dataName, imdb, setIx, featureDb, ...
        clusters, repetitions, covarianceBound, ...
        svmCost, kernelType, ...
        sampleRatePercent);
  
    results.results{setIx}.predicted_labels_train = predicted_labels_train;
    results.results{setIx}.accuracy_train = accuracy_train;
    results.results{setIx}.prob_estimates_train = prob_estimates_train;
    results.results{setIx}.predicted_labels_test = predicted_labels_test;
    results.results{setIx}.accuracy_test = accuracy_test;
    results.results{setIx}.prob_estimates_test = prob_estimates_test;
    results.results{setIx}.confusionResults = confusionResults;
    
end

saveResults('../Data', results, dataName, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

