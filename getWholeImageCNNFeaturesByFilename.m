function features = getWholeImageCNNFeaturesByFilename( dataRoot, imageRoot, ...
    datasetName, netName, imdb, imageMean, net)

    dataFilename = sprintf('features_wi_%s_%s.mat', datasetName, netName);
    
    dataFilename = fullfile(dataRoot, dataFilename);
    
    if (exist(dataFilename, 'file') == 2)
        features = load(dataFilename);
    else
        fprintf('Computing WholeImage-CNN features (%s)...\n', netName);
        
        numImages = length(imdb.images);        
       
        allCnnFeaturesCells = cell(1, numImages);
        
        imageFilenames = imdb.images;
        
        parfor i = 1:numImages
            
            image = imread(fullfile(imageRoot, imageFilenames{i}.filename));
            
            image = getCNNCompatImage(image);            
            
            image = image - imageMean;
        
            cnnFeatures = extractCNNWholeImage(net, netName, image);
            
            allCnnFeaturesCells{i} = cnnFeatures;

        end
        
        features = {};
        features.features = allCnnFeaturesCells;
        
        save(dataFilename, '-struct', 'features', '-v7.3');
    end

end

