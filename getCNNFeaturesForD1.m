function cnnImageFeatures = getCNNFeaturesForD1(numberOfImages, imageUncroppedData, newNet)
allImageMean = mean(imageUncroppedData.imagemean) ;
imagePath = '../KLD_TF/Images';
if ~exist('data/cnnfeatures.mat','file')
    for i = 1:numberOfImages
        imagename = imageUncroppedData.names{i};
        image = imread(fullfile(imagePath, imagename));     
        image = im2single(image);        
        fprintf('Subtracting Mean for cropped image\n');
        image = image - allImageMean;
        [r c m] = size(image);       
        if m < 3
            image = cat(3, image, image, image);
        end
        netres = vl_simplenn(newNet, image);
        imageFeatures = netres(20).x;
        % fetch CNN features 
        fprintf(strcat('Fetching CNN features for image',num2str(i),'\n'));        
        cnnImageFeatures{i} = reshape(imageFeatures,size(imageFeatures,1)*size(imageFeatures,2),size(imageFeatures,3));
    end
else
    fprintf(strcat('Loading CNN features for images from file','\n'));
    cnnImageFeatures = load('data/cnnfeatures.mat');
    cnnImageFeatures = cnnImageFeatures.cnnImageFeatures;
end