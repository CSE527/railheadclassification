function [ compactResults ] = showCrossValidationResults( results )
%SHOWCROSSVALIDATIONRESULTS Summary of this function goes here
%   Detailed explanation goes here

compactResults = {};
compactResults.sets = zeros(4, 4, 'double');

for i=1:4
    compactResults.sets(i, :) = getResultsForSet(results, i);    
end

compactResults.averageTraining = mean(compactResults.sets(:, 1));
compactResults.averageTesting = mean(compactResults.sets(:, 2));
compactResults.averageTriDiagonal = mean(compactResults.sets(:, 3));
compactResults.averageBandDistance = mean(compactResults.sets(:, 4));

fprintf('Average TRAINING: %f%%, Average TESTING: %f%%, Average TRI-DIAGONAL ACCURACY: %f%%, Average BAND DISTANCE: %f\n', ...
    compactResults.averageTraining, compactResults.averageTesting, ...
    compactResults.averageTriDiagonal, compactResults.averageBandDistance);

compactResults.sets

end

function values = getResultsForSet(results, i)
    values = zeros(1, 4);
    values(1,1) = results.results{i}.accuracy_train(1);
    values(1,2) = results.results{i}.accuracy_test(1);
    values(1,3) = results.results{i}.confusionResults.triDiagAccuracy * 100;
    values(1,4) = results.results{i}.confusionResults.bandDistance;
end