function newNet = loadPretrainedModel(root)
fprintf('Loading VGG-M...\n');
vggnet = load(strcat(root,'\imagenet-vgg-m.mat'));
newNet = vggnet;
for i = 1: numel(vggnet.layers)
    if strcmp(vggnet.layers{1,i}.type,'conv')
        newNet.layers{1,i} = rmfield(vggnet.layers{1,i},'weights');
        newNet.layers{1,i}.filters = vggnet.layers{1,i}.weights{1,1};
        newNet.layers{1,i}.biases = vggnet.layers{1,i}.weights{1,2};
    end
end
fprintf('Loading VGG-M completed! \n');
end