function [ outputImage ] = getCNNCompatImage( inputImage )
%GETCNNCOMPATIMAGE Returns a CNN-compatible input image.
% The CNNs used in this project require a hxwx3 single matrix
% for each image.

    d = size(inputImage, 3);

    if (~isfloat(inputImage) || (d < 3))
        outputImage = im2single(inputImage);
        
        if (d < 3)
           outputImage = cat(3, outputImage, outputImage, outputImage); 
        end        
        
    else
       outputImage = inputImage; 
    end

end

