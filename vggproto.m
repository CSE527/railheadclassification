% Prototype of connecting VGG-M to Fisher Vector to SVM

tic 
% Load VGG-M

fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');

% Load images
imageFiles = {
    '1_1_s.bmp', ...
    '2_1_s.bmp', ...
    '3_1_s.bmp', ...
    '4_1_s.bmp', ...
    '5_1_s.bmp', ...
    '6_1_s.bmp'};

% Tunable parameters
numClusters = 3; %45 
numRepetitions = 2; %20
covarianceBound = 10e-5; %10e-5
fisherNormalization = 'Improved'; % Normalized, SquareRoot, Improved, Fast

% Load every image into memory
imdb.labels = [1;2;3;4;5;6];
images = cell(length(imageFiles), 1);
for i = 1:length(images)
    images{i} = imread(fullfile('../Data/KLD_TF/Images', imageFiles{i}));    
end

% Normalize image sizes
imagey = 1000000;
imagex = 1000000;
for i = 1:length(images)
   im = images{i};
   imagey = min(imagey, size(im, 1));
   imagex = min(imagex, size(im, 2));
end

imdb.images = cell(length(images), 1);

% imdb.images = zeros(length(images), imagey, imagex, 3, 'single');

for i = 1:length(images)
    im = images{i};
   
    image = cropImage(im, imagey, imagex);   
    image = im2single(image);
    
    imdb.images{i} = image;
end

% Compute mean and subtract from each image

%imageMean = mean(imdb.images);
%imdb.images = imdb.images - imageMean;
allCnnFeaturesExists = 0;

if (exist('allCnnFeatures', 'var') == 0)

    %allCnnFeatures = zeros(length(imdb.images), 31*10*4096, 'single');

    for i = 1: length(imdb.images)

        image = imdb.images{i};

        % Extract the features from the CNN
        netres = vl_simplenn(net, image);
        imageFeatures = netres(20).x;
        
        %------------------ Vectorize-by-row ------------------
        %cnnFeatures = reshape(imageFeatures, 1, 310*4096);
        %------------------------------------------------------

        %------------------ Concat-by-row ------------------
        cnnFeatures = reshape(imageFeatures, 310, 4096);
        %------------------------------------------------------
        
        if (allCnnFeaturesExists == 0)
            allCnnFeatures = cnnFeatures;
            allCnnFeaturesExists = 1;
        else
            allCnnFeatures = cat(1, allCnnFeatures, cnnFeatures);
        end

    end
end

if (exist('covariances', 'var') == 0)
    
    % Compute the GMM all in one shot
    fprintf('Computing GMM...\n');
    
    [means, covariances, priors] = vl_gmm(allCnnFeatures, ...
        numClusters, ...
        'NumRepetitions', numRepetitions, ...
        'CovarianceBound', covarianceBound);
end

% fisherFeatureSize = 1269760;
% fisherFeatures = zeros(length(imageFiles), fisherFeatureSize, 'double');

%fisherFeatures = zeros(length(imageFiles), 4096, 'single');
fisherFeaturesCreated = 0;

% fisherFeatures = vl_fisher(allCnnFeatures, means, covariances, priors);

for i=1:length(images)    
    
    %------------------ Concatenate method ------------------
    startRow = (i-1)*310 + 1;
    endRow = i*310;    
    %------------------------------------------------------
    
    % Vectorize to single row
    %startRow = i;
    %endRow = i;
    
    imageFeatures = allCnnFeatures(startRow:endRow, :);    
    
    %------------------ Vectorize-by-row gmm, Vectorize-by-row fisher -----
    %imageFeatures = allCnnFeatures(i, :);
    %------------------------------------------------------
    
    %------------------ Cat-by-row gmm, vect-by-row fisher ---------------
%     startRow = (i-1)*310 + 1;
%     endRow = i*310;    
%     imageFeatures = allCnnFeatures(startRow:endRow, :);
%     
%     imageFeatures = reshape(imageFeatures, 1, numel(imageFeatures));
    %------------------------------------------------------

    %------------------ Cat-by-row gmm, cat-by-row fisher ---------------
%     startRow = (i-1)*310 + 1;
%     endRow = i*310;    
%     imageFeatures = allCnnFeatures(startRow:endRow, :);    
    %------------------------------------------------------    
    
    %fisherInputFeatures = imageFeatures;
    %fisherInputFeatures = reshape(imageFeatures, 1, numel(imageFeatures));
    %fisherInputFeatures = reshape(imageFeatures, size(imageFeatures, 2), size(imageFeatures, 1));
 
    %imageFeatures = imageFeatures'; - FAILED means
    
    %imageFeatures = reshape(imageFeatures, numel(imageFeatures), 1); -
    %FAILED means
    
    %imageFeatures = imageFeatures; FAILED means
    
    % TEST ONLY - This selects out the means and covariances
    % for the original image. THIS WILL NOT WORK FOR TESTING!
    m = means(startRow:endRow, :);
    c = covariances(startRow:endRow, :);
    
    features = vl_fisher(imageFeatures, ...
            m, ...
            c, ...
            priors, fisherNormalization);
        
    features = reshape(features, 1, numel(features));
        
    if (fisherFeaturesCreated == 0)
        fisherFeatures = features;
        fisherFeaturesCreated = 1;
    else
        fisherFeatures = cat(1, fisherFeatures, features);
    end
    
    %fisherFeatures(i, :) = features;
end

%fisherFeatures(i, :) = reshape(features, [1 fisherFeatureSize]);

% Train SVM

fprintf('Training SVM...\n');

labels = double(imdb.labels);
fisherFeatures = double(fisherFeatures);

svmModel = svmtrain(labels, fisherFeatures);


toc