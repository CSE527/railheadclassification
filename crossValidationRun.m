function [predicted_labelTRAIN, accuracyTRAIN, prob_estimatesTRAIN,...
          predicted_labelTEST, accuracyTEST, prob_estimatesTEST] = crossValidationRun(fourfoldcounter,...
                            numberOfImages,...
                            imageUncroppedData,...
                            cnnImageFeatures,...
                            numClusters,...
                            numRepetitions,...
                            covarianceBound)
    fprintf(strcat('CrossValidation Test ',num2str(fourfoldcounter), '\n'));    
    trainingseverity = [];
    testingseverity = [];
    allCNNFeaturesForGMMRun = [];
    cnnImagesForTraining = {};
    trainCounter = 0;   
    for i = 1:numberOfImages
        setarray = imageUncroppedData.setlabels{i};        
        imageType = setarray(fourfoldcounter);
        if imageType == 1
            trainCounter = trainCounter + 1;
            cnnImagesForTraining{trainCounter} = cnnImageFeatures{i};           
            trainingseverity = [trainingseverity; imageUncroppedData.severity(i)];
        else            
            testingseverity = [testingseverity; imageUncroppedData.severity(i)];            
        end        
    end   
    fprintf(strcat('CrossValidation Test: Vertical concatenation of features!','\n'));    
    allCNNFeaturesForGMMRun = vertcat(cnnImagesForTraining{1:trainCounter});
    fprintf(strcat('Computing GMM for training images belonging to training set run = ',num2str(fourfoldcounter),'\n'));
    [means, covariances, priors] = vl_gmm(allCNNFeaturesForGMMRun', ...
                                          numClusters, ...
                                        'NumRepetitions', numRepetitions, ...
                                        'CovarianceBound', covarianceBound);
    normalization = 'Improved';
    rowStart = 1;    
    trainCounter = 1;
    testCounter = 1;
    fisherFeatures = [];
    trainfisherFeatures = [];
    testfisherFeatures = [];
    for i = 1:numberOfImages               
        fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),'. Computing Fisher Vectors for image ',num2str(i),'! \n'));
        fisherFeatures(i,:) = vl_fisher(cnnImageFeatures{i}', ...
                                     means, ...
                                     covariances, ...
                                     priors, ...
                                     normalization); 
        imageTypeArr = imageUncroppedData.setlabels{i};                                  
        imageType = imageTypeArr(fourfoldcounter);
        if (imageType == 1)
            trainfisherFeatures(trainCounter,:) = fisherFeatures(i,:);                                 
            trainCounter = trainCounter + 1;
        else            
            testfisherFeatures(testCounter,:) = fisherFeatures(i,:);                                 
            testCounter = testCounter + 1;
        end        
    end
    % Train SVM
    fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),' Training SVM','\n'));
    svmTrainingModel = svmtrain(trainingseverity, trainfisherFeatures);
    fprintf(strcat('Crossvalidation run = ',num2str(fourfoldcounter),' Predicting for training set','\n'));
    [predicted_labelTRAIN, accuracyTRAIN, prob_estimatesTRAIN] = svmpredict(trainingseverity, sparse(trainfisherFeatures), svmTrainingModel);        
    [predicted_labelTEST, accuracyTEST, prob_estimatesTEST] = svmpredict(testingseverity, sparse(testfisherFeatures), svmTrainingModel);                       
end