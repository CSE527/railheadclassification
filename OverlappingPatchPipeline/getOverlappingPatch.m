function [ patch ] = getOverlappingPatch( image, patchSize, height, width, stride )
% Overlapping Patch Prerpocessing
% input: 
%        image: original image data
%        patchSize: size of each patch (default = 224)
%        height: height of the image
%        width: width of the image
%        stride: parameter to be tuned
% output:
%        patch: patchSize by patchSize by (numberOfPatches * numberOfImages)

% number of patches in height
patchHeight = floor((height-patchSize-1)/stride)+1;
% number of patches in width
patchWidth = floor((width-patchSize-1)/stride)+1;

patch = zeros(patchSize,patchSize,patchHeight*patchWidth);
% process the image
for m = 1:1:patchHeight
    for n = 1:1:patchWidth
        patch(1:patchSize,1:patchSize,(m-1)*n+n) = image((m-1)*stride+1:(m-1)*stride+patchSize,(n-1)*stride+1:(n-1)*stride+patchSize);
    end;
end;