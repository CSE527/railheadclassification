% function to get cnn features for the D2 images
function cnnImageFeatures = getCNNFeaturesForD3(root, numberOfImages, imageUncroppedData, newNet)
allImageMean = mean(imageUncroppedData.imagemean) ;
imagePath = strcat(root,'/D3/images');
    fprintf(strcat('Loading CNN features for images of D3 from file','\n'));
    load(strcat(root,'/data/cnnfeaturesD3.mat'));
    cnnImageFeatures = {};
    cnnImageFeatures.features = features;
    cnnImageFeatures.featureIndex = featureIndex;
    cnnImageFeatures.severity = severity;
end