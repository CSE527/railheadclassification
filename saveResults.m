function [ ] = saveResults( dataRoot, results, dataName, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType)
%SAVERESULTS Saves the results of a train/test validation with the 
% provided parameters.

filename = sprintf('results_%s_%d_%d_%s_c%d_k%d.mat', dataName, ...
    clusters, repetitions, getNameForFloat(covarianceBound), svmCost, kernelType);

filename = fullfile(dataRoot, filename);

save(filename, '-struct', 'results', '-v7.3');

end

