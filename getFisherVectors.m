function [ fisherVectors ] = getFisherVectors( featureDb, means, covariances, priors, normalization )
%GETFISHERVECTORS Returns the fisher vectors for the provided features,
% means, covariances and priors.
% featureDb - must be a cell containing a 2-D array of features
%             and a cell of {startIx, endIx} indicating which
%             features come from which images.

fisherVectorsCreated = 0;

for i = 1:length(featureDb.featureIndexes)
    
    indexes = featureDb.featureIndexes{i};
    
    startCol = indexes.startIx;
    endCol = indexes.endIx; 
    
    inputFeatures = featureDb.features(:, startCol:endCol);
   
    fisherFeatures = vl_fisher(inputFeatures, ...
        means, ...
        covariances, ...
        priors, ...
        normalization);
    
    fisherVector = reshape(fisherFeatures, 1, numel(fisherFeatures));
    
    if (fisherVectorsCreated == 0)
        fisherVectorsCreated = 1;
        fisherVectors = fisherVector;
    else
        fisherVectors = cat(1, fisherVectors, fisherVector);
    end
end


end

