function [ predicted_labels_train, accuracy_train, prob_estimates_train, ...
           predicted_labels_test, accuracy_test, prob_estimates_test, ...
           confusionMatrix] = trainAndTest( dataRoot, dataName, imdb, setIx, cnnFeatureDb, ...
           clusters, repetitions, covarianceBound, svmCost, kernelType, sampleRatePercent )
%CROSSVALIDATE Runs a single cross validation on dataset D1 

    trainingSet = imdb.sets{setIx}.train;
    testingSet = imdb.sets{setIx}.test;
   
    numTrainingImages = numel(trainingSet);
    numTestingImages = numel(testingSet);
    
    % Build the GMM 
  
    % Calculate the size of the training feature matrix
    numTrainingFeatures = 0;
    for i=1:numTrainingImages
        numTrainingFeatures = numTrainingFeatures + size(cnnFeatureDb.features{trainingSet(i)}, 1);
    end
    
    if (sampleRatePercent <= 0 || sampleRatePercent > 100)
       error('Invalid sampleRatePercent specified: %d', sampleRatePercent); 
    end
    
    % Build the training feature matrix
    if (sampleRatePercent == 100)
    
        features = zeros(size(cnnFeatureDb.features{1}, 2), numTrainingFeatures, 'single');

        featureIx = 1;

        for i=1:numTrainingImages

            endIx = featureIx + size(cnnFeatureDb.features{trainingSet(i)}, 1) - 1;

            features(:, featureIx:endIx) = cnnFeatureDb.features{trainingSet(i)}(:, 1:end)';

            featureIx = endIx + 1;
        end
    else
        % Down-sample according to the specified rate
        severities = cellfun(@(x) x.severity, imdb.images, 'UniformOutput', true);
        severityTotals = histc(severities, unique(severities));
        
        severityCounts = ceil(severityTotals * (sampleRatePercent / 100));
        
        featureCells = cell(1, 1);
        
        featureCellIx = 1;
        for i=1:numTrainingImages
            imageIx = trainingSet(i);
            severity = imdb.images{imageIx}.severity;
            
            if (severityCounts(severity) == 0)
                continue;
            end
            
            severityCounts(severity) = severityCounts(severity) - 1;
            
            featureCells{featureCellIx} = cnnFeatureDb.features{imageIx}';
            featureCellIx = featureCellIx + 1;
            
            if (sum(severityCounts) == 0)
                break;
            end            
        end
        
        features = cell2mat(featureCells);
    end
    
    fprintf('Computing GMM for training set %d...\n', setIx);

    if (sampleRatePercent == 100)
        gmmName = sprintf('%s_%d', dataName, setIx); 
    else
        gmmName = sprintf('%s_%d_s%d', dataName, setIx, sampleRatePercent);
    end
    
    [means, covariances, priors] = getGMM(dataRoot, gmmName, features, ...
        clusters, repetitions, covarianceBound );
    
    clearvars features;
    
    fprintf('Computing Fisher Vectors...\n');    
    
    trainingFisherVectors = getFisherVectorsForSet(cnnFeatureDb, trainingSet, ...
        means, covariances, priors, 'Improved');
    
    trainingFisherVectors = double(trainingFisherVectors);
    
    fprintf('Training SVM...\n');
    
    trainingLabels = zeros(numTrainingImages, 1, 'double');
    for i=1:numTrainingImages
        trainingLabels(i) = imdb.images{trainingSet(i)}.severity;
    end

    % Train the SVM with a linear kernel.
    svmModel = svmtrain(trainingLabels, trainingFisherVectors, sprintf('-t %d -c %d', kernelType, svmCost));
    
    % Get training Accuracy
    fprintf('Computing training accuracy...\n');
    [ predicted_labels_train, accuracy_train, prob_estimates_train ] = svmpredict(trainingLabels, ...
        trainingFisherVectors, svmModel);
    
    % Test
    fprintf('Testing. Computing Fisher Vectors...\n');
    testingFisherVectors = getFisherVectorsForSet(cnnFeatureDb, testingSet, ...
        means, covariances, priors, 'Improved');  
    
    testLabels = zeros(numTestingImages, 1, 'double');
    for i=1:numTestingImages
       testLabels(i) = imdb.images{testingSet(i)}.severity; 
    end
    
    testingFisherVectors = double(testingFisherVectors);    
    
    fprintf('Testing with SVM...\n');
    [ predicted_labels_test, accuracy_test, prob_estimates_test] = svmpredict(testLabels, ...
        testingFisherVectors, svmModel);
    
    confusionMatrix = computeConfusionResult(predicted_labels_test, testLabels, 8);
    
    fprintf('*** Training Accuracy = %f%%, Testing Accuracy = %f%%\n', accuracy_train(1), accuracy_test(1));
end

