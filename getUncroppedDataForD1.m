function [numberOfImages imageUncroppedData] = getUncroppedDataForD1()
    if exist('data/uncroppedimagesD1.mat','file')
        fprintf('loading saved uncroppedimagesD1.mat. Preprocessing...\n');
        imageUncroppedData = load('data/uncroppedimagesD1.mat');       
    else        
        fprintf('uncroppedimagesD1.mat does not exist! Preprocessing...\n');
        imagePath = '../KLD_TF/Images';
        imageFiles = dir(fullfile(imagePath, '*.bmp'));
        imageUncroppedData={};
        imageUncroppedData.severity= zeros(939,1);
        imageUncroppedData.imagemean = [];
        numberOfImages = size(imageFiles,1);
        numberOfImages = 6;
        % 4 fold cross validation set preparation
        % First Set
        train1Str = fileread('../Train1.txt');   %read entire file into string
        train1parts = strtrim(regexp( train1Str, '(\r|\n)+', 'split'));  %split by each line    
        test1Str = fileread('../Test1.txt');   %read entire file into string
        test1parts = strtrim(regexp( test1Str, '(\r|\n)+', 'split'));  %split by each line

        % Second Set
        train2Str = fileread('../Train2.txt');   %read entire file into string
        train2parts = strtrim(regexp( train2Str, '(\r|\n)+', 'split'));  %split by each line    
        test2Str = fileread('../Test2.txt');   %read entire file into string
        test2parts = strtrim(regexp( test2Str, '(\r|\n)+', 'split'));  %split by each line

        % Third Set
        train3Str = fileread('../Train3.txt');   %read entire file into string
        train3parts = strtrim(regexp( train3Str, '(\r|\n)+', 'split'));  %split by each line    
        test3Str = fileread('../Test3.txt');   %read entire file into string
        test3parts = strtrim(regexp( test3Str, '(\r|\n)+', 'split'));  %split by each line

        % Fourth Set
        train4Str = fileread('../Train4.txt');   %read entire file into string
        train4parts = strtrim(regexp( train4Str, '(\r|\n)+', 'split'));  %split by each line    
        test4Str = fileread('../Test4.txt');   %read entire file into string
        test4parts = strtrim(regexp( test4Str, '(\r|\n)+', 'split'));  %split by each line        
        fclose('all');
        %sumOfTrainMeans = zeros(1,4);
        %sumOfTestMeans = zeros(1,4);
        for j = 1: numberOfImages
            imagename = imageFiles(j).name;
            severity = getSeverityFromFilename(imagename);
            image = imread(fullfile(imagePath, imagename));        
            image = im2single(image);
            [height, width, m] = size(image);                 
            sets = getSetLabelForFileNameFor4FoldCrossValidation(train1parts, test1parts, ...
                                                     train2parts, test2parts, ...
                                                     train3parts, test3parts, ...
                                                     train4parts, test4parts, ...
                                                     imagename);
            currentImageMean = mean(mean(image));
            imageUncroppedData.imagemean(j) = currentImageMean;
            imageUncroppedData.names{j} = imagename;
            imageUncroppedData.severity(j) = severity;                                                 
            imageUncroppedData.setlabels{j} = sets;            
        end
        fprintf('saving uncroppedimagesD1.mat. Preprocessing...\n');
        %save('data/uncroppedimagesD1.mat', '-struct', 'imageUncroppedData') ;
    end
end