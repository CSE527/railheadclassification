function [ croppedImage ] = cropImage( image, outSizeX, outSizeY)
%CROPIMAGE Crops a single image to the desired size

    [r,c,m] = size(image);  
    if m < 3
        image = cat(3, image,image,image);
    end
    t = max(r,c);
    template = 255*ones(r,c,3,'uint8');
    if r < c 
        template(floor((c-r)/2)+1:floor((c-r)/2)+r,:,:) = image;
    else
        template(:,floor((r-c)/2)+1:floor((r-c)/2)+c,:) = image;
    end
    croppedImage = imresize(template, [outSizeX, outSizeY]);

end

