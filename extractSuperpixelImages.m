function [ segmentImages, segments ] = extractSuperpixelImages( image, regionSize, regularizer )
%EXTRACTSUPERPIXELIMAGES Extracts superpixels for the image provided.

segments = vl_slic(image, regionSize, regularizer );

maxSegmentId = max(segments(:));

segmentImages = cell(1, 1);

segmentImageIx = 1;

for segment = 1:maxSegmentId
    % Find pixels belonging to current label. currentSegment will be
    % logical where each pixel in the segment is "true".
    
    currentSegment = segments == segment; 

    [iy, ix] = find(currentSegment);
    
    width = max(ix) - min(ix) + 1;
    height = max(iy) - min(iy) + 1; 
    
    segmentImage = zeros(height, width, size(image, 3), 'single');

    if (size(segmentImage, 1) == 0 || size(segmentImage, 2) == 0)
        continue;
    else    
       
        miny = min(iy);
        maxy = max(iy);
        minx = min(ix);
        maxx = max(ix);

        segmentImage(1:height, 1:width, :) = image(miny:maxy, minx:maxx, :);

        segmentImages{segmentImageIx} = segmentImage;

        segmentImageIx = segmentImageIx + 1;
    end
end

end

