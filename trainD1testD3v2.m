
fprintf('Training with D1 and testing with D3 using whole image.\n');
clearvars;

dataRoot = '../Data';
netType = 'vggm';
clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;
svmCost = 2;
kernelType = 0; % Linear
sampleRatePercent = 100;

fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');

fprintf('Loading D1 dataset...\n');
imdb1 = getD1DatasetByFilename(dataRoot);

% Get the features for the D1 dataset
featureDb1 = getWholeImageCNNFeaturesByFilename(dataRoot, '../Data/KLD_TF/Images', ...
    'D1', netType, imdb1, imdb1.imageMean, net);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Assemble the training features

trainingSet = imdb1.allImageSet.train;
numTrainingImages = numel(trainingSet);

numTrainingFeatures = 0;
for i=1:numTrainingImages
    numTrainingFeatures = numTrainingFeatures + size(featureDb1.features{trainingSet(i)}, 1);
end

features = zeros(size(featureDb1.features{1}, 2), numTrainingFeatures, 'single');

featureIx = 1;

for i=1:numTrainingImages

    endIx = featureIx + size(featureDb1.features{trainingSet(i)}, 1) - 1;

    features(:, featureIx:endIx) = featureDb1.features{trainingSet(i)}(:, 1:end)';

    featureIx = endIx + 1;
end

fprintf('Computing GMM for training set D1...\n');

gmmName = 'wi_D1_vggm'; 

[means, covariances, priors] = getGMM(dataRoot, gmmName, features, ...
    clusters, repetitions, covarianceBound );

clearvars features;

fprintf('Computing Fisher Vectors...\n');    

trainingFisherVectors = getFisherVectorsForSet(featureDb1, trainingSet, ...
    means, covariances, priors, 'Improved');

clearvars featureDb1;

trainingFisherVectors = double(trainingFisherVectors);

fprintf('Training SVM...\n');

trainingLabels = zeros(numTrainingImages, 1, 'double');
for i=1:numTrainingImages
    trainingLabels(i) = imdb1.images{trainingSet(i)}.severity;
end

% Train the SVM with a linear kernel.
svmModel = svmtrain(trainingLabels, trainingFisherVectors, sprintf('-t %d -c %d', kernelType, svmCost));

% Get training Accuracy
fprintf('Computing training accuracy...\n');
[ predicted_labels_train, accuracy_train, prob_estimates_train ] = svmpredict(trainingLabels, ...
    trainingFisherVectors, svmModel);

clearvars trainingFisherVectors;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('TESTING...\n');
fprintf('Loading D3 dataset...\n');
imdb3 = getD3DatasetByFilename(dataRoot);

% Get the features for the D3 dataset
featureDb3 = getWholeImageCNNFeaturesByFilename(dataRoot, '../Data/D3', ...
    'D3', netType, imdb3, imdb3.imageMean, net);

testingSet = imdb3.allImageSet.train;

fprintf('Computing Fisher Vectors...\n');
testingFisherVectors = getFisherVectorsForSet(featureDb3, testingSet, ...
    means, covariances, priors, 'Improved');  

numTestingImages = numel(testingSet);

testLabels = zeros(numTestingImages, 1, 'double');
for i=1:numTestingImages
   testLabels(i) = imdb3.images{testingSet(i)}.severity; 
end

testingFisherVectors = double(testingFisherVectors);    

fprintf('Testing with SVM...\n');
[ predicted_labels_test, accuracy_test, prob_estimates_test] = svmpredict(testLabels, ...
    testingFisherVectors, svmModel);

confusionMatrix = computeConfusionResult(predicted_labels_test, testLabels, 8);

fprintf('*** Training Accuracy = %f%%, Testing Accuracy = %f%%\n', accuracy_train(1), accuracy_test(1));

saveResults(dataRoot, confusionMatrix, 'trainD1TestD3_wi_vggm', ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);


