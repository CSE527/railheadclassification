function [ fullPath ] = getFullPath( relativePath )
%GETFULLPATH Returns the fully qualified path of relativePath

curDir = pwd;

[pathstr, name, ext] = fileparts(relativePath);

cd(pathstr);

fqDir = pwd;

cd(curDir);

fullPath = fullfile(fqDir, [name ext]);

end

