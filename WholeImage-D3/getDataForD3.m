% function to load D2 dataset images and their severity labels
function [numberOfImages imageUncroppedData] = getDataForD3(root)
    if exist(strcat(root,'/data/uncroppedimagesD3.mat'),'file')
        fprintf('loading saved uncroppedimagesD3.mat. Preprocessing...\n');
        imageUncroppedData = load(strcat(root,'/data/uncroppedimagesD3.mat'));       
        numberOfImages = size(imageUncroppedData.names, 2);
    else        
        fprintf('uncroppedimagesD3.mat does not exist! Preprocessing...\n');
        imagePath = strcat(root,'\D3\images');
        imageFiles = dir(fullfile(imagePath, '*.bmp'));
        imageUncroppedData={};
        imageUncroppedData.severity= zeros(160,1);
        imageUncroppedData.imagemean = [];
        numberOfImages = size(imageFiles,1);        
        fclose('all');
        %sumOfTrainMeans = zeros(1,4);
        %sumOfTestMeans = zeros(1,4);
        for j = 1: numberOfImages
            imagename = imageFiles(j).name;
            severity = getSeverityFromFilenameD3(imagename);
            image = imread(fullfile(imagePath, imagename));        
            image = im2single(image);
            [height, width, m] = size(image);                           
            currentImageMean = mean(mean(image));
            imageUncroppedData.imagemean(j) = currentImageMean;
            imageUncroppedData.names{j} = imagename;
            imageUncroppedData.severity(j) = severity;                                                          
        end
        fprintf('saving uncroppedimagesD3.mat. Preprocessing...\n');
        save(strcat(root,'/data/uncroppedimagesD3.mat'), '-struct', 'imageUncroppedData') ;
    end
end