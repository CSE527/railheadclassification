function features = getSuperpixelCNNFeaturesByFilename( dataRoot, imageRoot, netName, ...
    imdb, imageMean, net, regionSize, regularizer)

    regName = getNameForFloat(regularizer);
    
    dataFilename = sprintf('features_sp_%s_%d_%s.mat', netName, regionSize, regName);
    
    dataFilename = fullfile(dataRoot, dataFilename);
    
    if (exist(dataFilename, 'file') == 2)
        features = load(dataFilename);
    else
        fprintf('Computing Superpixel-CNN features (%s, %d, %f)...\n', ...
            netName, regionSize, regularizer);
        
        % allCnnFeaturesCreated = 0;
        numImages = length(imdb.images);        
        imageCnnFeaturesIndexes = cell(numImages, 1);
        
        allCnnFeaturesCells = cell(1, numImages);
        
        tic
        
%         featureIx = 1;
%         for i = 1:numImages
%             fprintf('Processing image %d of %d...\n', i, numImages);
%             
%             image = imread(fullfile(imageRoot, imdb.images{i}.filename));
%             
%             image = getCNNCompatImage(image);            
%             
%             image = image - imageMean;
%         
%             cnnFeatures = extractCNNSuperPixel(net, netName, image, regionSize, regularizer);
% 
%             indexes = {};
%             indexes.startIx = featureIx;
%             indexes.endIx = indexes.startIx + size(cnnFeatures, 2) - 1;
%             featureIx = indexes.endIx + 1;
%             imageCnnFeaturesIndexes{i} = indexes;
%             
%             allCnnFeaturesCells{i} = cnnFeatures;
% 
%         end

        imageFilenames = imdb.images;

        parfor i = 1:numImages
            %fprintf('Processing image %d of %d...\n', i, numImages);
            
            image = imread(fullfile(imageRoot, imageFilenames{i}.filename));
            
            image = getCNNCompatImage(image);            
            
            image = image - imageMean;
        
            cnnFeatures = extractCNNSuperPixel(net, netName, image, regionSize, regularizer);

            allCnnFeaturesCells{i} = cnnFeatures;

        end
        
        % Construct feature indexes
        featureIx = 1;
        
        for i = 1:numImages
            indexes = {};
            indexes.startIx = featureIx;
            cnnFeatures = allCnnFeaturesCells{i};
            indexes.endIx = indexes.startIx + size(cnnFeatures, 2) - 1;
            featureIx = indexes.endIx + 1;
            imageCnnFeaturesIndexes{i} = indexes;
        end

        toc
        
        allCnnFeatures = cell2mat(allCnnFeaturesCells);
        
        features = {};
        features.featureIndexes = imageCnnFeaturesIndexes;
        features.features = allCnnFeatures;
        
        save(dataFilename, '-struct', 'features', '-v7.3');
    end

end

