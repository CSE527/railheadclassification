function [ cnnFeatures ] = extractCNNSuperPixel( net, netName, image, ...
    slicRegionSize, slicRegularizer)
%extractCNNSuperPixel Extracts cnn features for the image provided
%using the super-pixel submission method. 
   
    [ segmentImages, ~ ] = extractSuperpixelImages( image, ...
        slicRegionSize, slicRegularizer);
    
    numSegments = length(segmentImages);
        
    if ((strcmpi(netName, 'vggm') ~= 0) || (strcmpi(netName, 'vggs') ~= 0))
        featureSize = 4096;
    elseif (strcmpi(netName, 'googlenet') ~= 0)
        featureSize = 1000;
    else
        error('Unrecognized network name specified.'); 
    end
        
    allFeatures = zeros(featureSize, numSegments, 'single');
    
    for i = 1:numSegments
       
        image = segmentImages{i};
       
        % Extract features from network

        % Resize image for submission to CNN
        image = imresize(image, [224 224]);

        if (strcmpi(netName, 'vggm') ~= 0)
            cnnResult = vl_simplenn(net, image);
            features = cnnResult(20).x;
        elseif (strcmpi(netName, 'vggs') ~= 0) 
            cnnResult = vl_simplenn(net, image);
            features = cnnResult(19).x;            
        elseif (strcmpi(netName, 'googlenet') ~= 0)
            net.eval({'data', image});    
            features = net.vars(net.getVarIndex('prob')).value;
        else
           error('Unrecognized network name specified.'); 
        end
        
        features = reshape(features, numel(features), 1);

        allFeatures(:, i) = features;
    end    

    cnnFeatures = allFeatures;

end

