function cnnImageFeatures = getCNNFeaturesForD1(root, numberOfImages, imageUncroppedData, newNet)
allImageMean = mean(imageUncroppedData.imagemean) ;
imagePath = strcat(root,'/KLD_TF/Images');
if ~exist(strcat(root,'/data/cnnfeatures.mat'),'file')
    for i = 1:numberOfImages
        imagename = imageUncroppedData.names{i};
        image = imread(fullfile(imagePath, imagename));     
        image = im2single(image);        
        fprintf('Subtracting Mean for cropped image\n');
        image = image - allImageMean;
        [r c m] = size(image);       
        if m < 3
            image = cat(3, image, image, image);
        end
        % fetch CNN features 
        fprintf(strcat('Fetching CNN features for image',num2str(i),'\n')); 
        netres = vl_simplenn(newNet, image);
        imageFeatures = netres(20).x;       
        cnnImageFeatures{i} = reshape(imageFeatures,size(imageFeatures,1)*size(imageFeatures,2),size(imageFeatures,3));
    end
    %savecnn = tic;
    %fprintf('saving cnn features. Preprocessing...\n');
    save(strcat(root,'/data/cnnfeatures.mat'), 'cnnImageFeatures','-v7.3') ;
    %toc(savecnn);
else
    fprintf(strcat('Loading CNN features for images from file','\n'));
    cnnImageFeatures = load(strcat(root,'/data/cnnfeatures.mat'));
    cnnImageFeatures = cnnImageFeatures.cnnImageFeatures;
end