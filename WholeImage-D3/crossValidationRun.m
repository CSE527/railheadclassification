function [predicted_labelTRAIN, accuracyTRAIN, trainingseverity,...
          predicted_labelTEST, accuracyTEST, testingseverity] = crossValidationRun(...
                            numberOfImages,...
                            imageUncroppedData,...
                            cnnImageFeatures,...
                            numberOfTestImages,...
                            d2imagedata,...
                            cnnTestImageFeatures,...
                            means,...
                            covariances,...
                            priors,...
                            normalization,...
                            cost,...
                            kernel)    
    trainCounter = 1;
    testCounter = 1;
    fisherFeatures = [];
    xfisherFeatures = [];
    trainfisherFeatures = [];
    testfisherFeatures = [];
    trainingseverity = [];    
    testingseverity = [];
    fisherTrainFeatures = {};
    for i = 1:numberOfImages               
        fprintf(strcat('Train D1, Test D3. Computing Fisher Vectors for image ',num2str(i),'! \n'));
        fisherFeatures(i,:) = vl_fisher(cnnImageFeatures{i}', ...
                                     means, ...
                                     covariances, ...
                                     priors, ...
                                     normalization); 
        fisherTrainFeatures{i} = fisherFeatures(i,:);                         
        imageTypeArr = imageUncroppedData.setlabels{i};                                  
        trainfisherFeatures(trainCounter,:) = fisherFeatures(i,:);
        trainingseverity = [trainingseverity; imageUncroppedData.severity(i)];
        trainCounter = trainCounter + 1;        
    end            
    for j = 1:numberOfTestImages
        featureIndex = cnnTestImageFeatures.featureIndex(j,:);
        features = cnnTestImageFeatures.features(featureIndex(1):featureIndex(2),:);
        xfisherFeatures(j,:) = vl_fisher(features', ...
                                     means, ...
                                     covariances, ...
                                     priors, ...
                                     normalization);       
        testfisherFeatures(testCounter,:) = xfisherFeatures(j,:);
        testingseverity = [testingseverity; cnnTestImageFeatures.severity(j)];
        testCounter = testCounter + 1;        
    end
    % Train SVM
    fprintf(strcat('Train D1, Test D3: ', 'Training SVM','\n'));
    costkernel = '-t 0 -c 2';
    samplesize = int16(numberOfImages*0.75);
    severitylevels = [1 2 3 4 5 6 7 8]'; 
    [trainfisherFeatures,trainingseverity] = buildSampleCombinedFeatures(fisherTrainFeatures, trainingseverity, samplesize, severitylevels);
    svmTrainingModel = svmtrain(trainingseverity, trainfisherFeatures, costkernel);
    fprintf(strcat('Train D1, Test D3: ',' Predicting for training set','\n'));
    [predicted_labelTRAIN, accuracyTRAIN, prob_estimatesTRAIN] = svmpredict(trainingseverity, sparse(trainfisherFeatures), svmTrainingModel);        
    [predicted_labelTEST, accuracyTEST, prob_estimatesTEST] = svmpredict(testingseverity, sparse(testfisherFeatures), svmTrainingModel);                           
end