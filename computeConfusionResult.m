function [ confusionResult ] = computeConfusionResult( predictedLabels, actualLabels, matrixSize )
%COMPUTECONFUSIONMATRIX Computes the confusion matrix between actual
% and predicted label sets, plus:
% Accuracy - main diagonal / sum of all entries
% Tri-Diagonal - main diagonal, plus +1, -1 diagonals / sum of all entries
% Band Distance
% NOTE: predictedLabels and actualLabels must be single column vectors.

if (size(predictedLabels, 1) ~= size(actualLabels, 1))
    error('Label vectors don''t match');
end

% Build the matrix
confusionMatrix = zeros(matrixSize, matrixSize, 'uint32');

for i=1:size(predictedLabels, 1)
    p = predictedLabels(i);
    a = actualLabels(i);    
    
    confusionMatrix(p, a) = confusionMatrix(p, a) + 1;
    
end    

% Compute the accuracy

accSum = 0;
triSum = 0;
for i=1:8
    diag = confusionMatrix(i, i);
    accSum = accSum + diag;

    triSum = triSum + diag;
    if (i == 1)
        triSum = triSum + confusionMatrix(i, i+1);
    elseif (i == 8)
        triSum = triSum + confusionMatrix(i, i-1);
    else
        triSum = triSum + confusionMatrix(i, i+1) + confusionMatrix(i, i-1);
    end    
end

countOfEntries = sum(confusionMatrix(:));

confusionResult = {};
confusionResult.confusionMatrix = confusionMatrix;
confusionResult.accuracy = double(accSum) / double(countOfEntries);
confusionResult.triDiagAccuracy = double(triSum) / double(countOfEntries);
confusionResult.bandDistance = diagdist(double(confusionMatrix));

end

