function [ ] = validateImages( folderName )
%VALIDATEIMAGES Loads each image file in the indicated folder.

    failedImageCount = 0;
    imageFiles = dir(folderName);
    for j = 1: length(imageFiles)
        if imageFiles(j).name(1) == '.'
            continue
        else            
            try
                im = imread(fullfile(folderName, imageFiles(j).name));            
            catch
                failedImageCount = failedImageCount + 1;
                fprintf('Failed to load image %s\n', imageFiles(j).name);               
            end       
        end
    end
    
    if (failedImageCount > 0)
        fprintf('Failed to load %d images out of %d.\n', failedImageCount, length(imageFiles));
    else
        fprintf('All files successfully loaded.\n');
    end

end

