function [ imdb ] = getD3DatasetByFilename( dataRoot )
%GETD1DATASET Returns an in-memory database of the D3 dataset.
%   The structure will be as follows:
% images - cell of structures, each containing filename, severity
%          and imageMean.
% sets - a 4-member cell containing structures, each containing:
%        train - a set of image indices to the training images
%        test - a set of image indices to the test images
%        trainingMean - the mean of the training images.
% imageMean - the mean of all images.

    datafilePath = fullfile(dataRoot, 'D3Data.mat');

    if exist(datafilePath,'file')
        imdb = load(datafilePath);       
    else        
        imagePath = fullfile(dataRoot, 'D3\Images');
        imageFiles = dir(fullfile(imagePath, '*.bmp'));

        imdb = {};    
        numberOfImages = size(imageFiles,1);
        imdb.images = cell(1, numberOfImages);
        imdb.sets = cell(1, 4);

        sum = 0;
        
        for i = 1:numberOfImages
            imagename = imageFiles(i).name;
            severity = getSeverityFromFilenameD3(imagename);
            
            if (severity < 1 || severity > 8)
                error('Invalid severity encountered for file %s', imagename);
            end
            
            image = imread(fullfile(imagePath, imagename));        
           
            imdb.images{i}.filename = imagename;                                    
            image = im2single(image);            
            
            imdb.images{i}.imageMean = mean(image(:));
            imdb.images{i}.severity = severity;
            
            sum = sum + imdb.images{i}.imageMean;

        end
        
        imdb.imageMean = sum / numberOfImages;
        
        % Save a set containing every image
        imdb.allImageSet.trainingMean = imdb.imageMean;
        imdb.allImageSet.train = 1:numberOfImages;        

        
        % 4 fold cross validation set preparation
        % For each set, take 75% of each severity as training and 
        % 25% of each severity as testing

        severities = cellfun(@(x) x.severity, imdb.images, 'UniformOutput', true);
        severityTotals = histc(severities, unique(severities));
        testingCounts = floor(severityTotals / 4);

        % Build a map from an index into the severity class
        % back to an image index
        severityToImageIndexes = cell(8, 1);
        for i = 1:numberOfImages
            severityToImageIndexes{imdb.images{i}.severity} = cat(1, severityToImageIndexes{imdb.images{i}.severity}, i);            
        end
       
        % Build the testing indexes for each cross-validation
        % to guarantee no overlap between testing and training.
        testingIndexes = zeros(8, 4, 2, 'uint32');
        
        for i = 1:8
            testingIndexes(i, 1, 1) = 1;
            testingIndexes(i, 1, 2) = testingCounts(i);
            
            testingIndexes(i, 2, 1) = testingIndexes(i, 1, 2) + 1;
            testingIndexes(i, 2, 2) = testingIndexes(i, 2, 1) + testingCounts(i) - 1;
            
            testingIndexes(i, 3, 1) = testingIndexes(i, 2, 2) + 1;
            testingIndexes(i, 3, 2) = testingIndexes(i, 3, 1) + testingCounts(i) - 1;

            testingIndexes(i, 4, 1) = testingIndexes(i, 3, 2) + 1;
            testingIndexes(i, 4, 2) = testingIndexes(i, 4, 1) + testingCounts(i) - 1;            
        end
       
        imageIndexes = 1:numberOfImages;

        % First Set
        imdb.sets{1}.test = [];

        for i=1:8
            imdb.sets{1}.test = cat(1, imdb.sets{1}.test, severityToImageIndexes{i}(testingIndexes(i, 1, 1):testingIndexes(i, 1, 2)));
        end        
        imdb.sets{1}.train = setdiff(imageIndexes, imdb.sets{1}.test);
        
        % Second Set
        imdb.sets{2}.test = [];

        for i=1:8
            imdb.sets{2}.test = cat(1, imdb.sets{2}.test, severityToImageIndexes{i}(testingIndexes(i, 2, 1):testingIndexes(i, 2, 2)));
        end
        imdb.sets{2}.train = setdiff(imageIndexes, imdb.sets{2}.test);

        % Third Set
        imdb.sets{3}.test = [];

        for i=1:8
            imdb.sets{3}.test = cat(1, imdb.sets{3}.test, severityToImageIndexes{i}(testingIndexes(i, 3, 1):testingIndexes(i, 3, 2)));
        end
        imdb.sets{3}.train = setdiff(imageIndexes, imdb.sets{3}.test);

        % Fourth Set
        imdb.sets{4}.test = [];

        for i=1:8
            imdb.sets{4}.test = cat(1, imdb.sets{4}.test, severityToImageIndexes{i}(testingIndexes(i, 4, 1):testingIndexes(i, 4, 2)));
        end
        imdb.sets{4}.train = setdiff(imageIndexes, imdb.sets{4}.test);

        imdb.sets{1}.trainingMean = computeMean(imdb.images, imdb.sets{1}.train);
        imdb.sets{2}.trainingMean = computeMean(imdb.images, imdb.sets{2}.train);
        imdb.sets{3}.trainingMean = computeMean(imdb.images, imdb.sets{3}.train);
        imdb.sets{4}.trainingMean = computeMean(imdb.images, imdb.sets{4}.train);        

        save(datafilePath, '-struct', 'imdb', '-v7.3') ;
    end    
end


function [ imageMean ] = computeMean(images, indexes)
    sum = 0;
    count = size(indexes, 2);
    for i=1:count
        %sum = sum + mean(images{indexes(i)}.data(:));
        sum = sum + images{indexes(i)}.imageMean;
    end

    imageMean = sum / count;
end

%function [ trainingIndexes ] = getTrainingIndexes(severities, 