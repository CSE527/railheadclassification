% function to load D2 dataset images and their severity labels
function [numberOfImages imageUncroppedData] = getDataForD2(root)
    if exist(strcat(root,'/data/uncroppedimagesD2.mat'),'file')
        fprintf('loading saved uncroppedimagesD2.mat. Preprocessing...\n');
        imageUncroppedData = load(strcat(root,'/data/uncroppedimagesD2.mat'));       
        numberOfImages = size(imageUncroppedData.names, 2);
    else        
        fprintf('uncroppedimagesD2.mat does not exist! Preprocessing...\n');
        imagePath = strcat(root,'\D2\img');
        imageFiles = dir(fullfile(imagePath, '*.jpeg'));
        imageUncroppedData={};
        imageUncroppedData.severity= zeros(160,1);
        imageUncroppedData.imagemean = [];
        numberOfImages = size(imageFiles,1);        
        fclose('all');
        %sumOfTrainMeans = zeros(1,4);
        %sumOfTestMeans = zeros(1,4);
        for j = 1: numberOfImages
            imagename = imageFiles(j).name;
            severity = getSeverityFromFilename(imagename);
            image = imread(fullfile(imagePath, imagename));        
            image = im2single(image);
            [height, width, m] = size(image);                           
            currentImageMean = mean(mean(image));
            imageUncroppedData.imagemean(j) = currentImageMean;
            imageUncroppedData.names{j} = imagename;
            imageUncroppedData.severity(j) = severity;                                                          
        end
        fprintf('saving uncroppedimagesD2.mat. Preprocessing...\n');
        save(strcat(root,'/data/uncroppedimagesD2.mat'), '-struct', 'imageUncroppedData') ;
    end
end