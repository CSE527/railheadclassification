function googlenet = loadPretrainedModel(root)
fprintf('Loading GoogleNet...\n');
googlenet = dagnn.DagNN.loadobj(load(strcat(root,'\imagenet-googlenet-dag.mat')));
%googlenet = load();
fprintf('Loading GoogleNet completed! \n');
end