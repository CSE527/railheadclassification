function [ features ] = extractCNNWholeImage( net, netName, image )
%extractCNNWholeImage Extracts cnn features for the image 
   
%     if ((strcmpi(netName, 'vggm') ~= 0) || (strcmpi(netName, 'vggs') ~= 0))
%         featureSize = 4096;
%     elseif (strcmpi(netName, 'googlenet') ~= 0)
%         featureSize = 1000;
%     else
%         error('Unrecognized network name specified.'); 
%     end
       
    % Extract features from network
    
    d = min(size(image, 1), size(image, 2));
    
    if (d < 224)
        % Pad the image to be compatible with the CNN
         if (size(image, 1) < 224)
            newImage = zeros(224, size(image, 2), size(image, 3), 'single');
         else
            newImage = zeros(size(image, 1), 224, size(image, 3), 'single');             
         end 
         
         newImage(1:size(image, 1), 1:size(image, 2), :) = image(1:size(image, 1), 1:size(image, 2), :);
         
         image = newImage;
        
        % Resize the image to be compatible with the CNN
%         r = 224 / d;
%         image = imresize(image, [size(image, 1) * r, size(image, 2) * r]);
    end

    if (strcmpi(netName, 'vggm') ~= 0)
        cnnResult = vl_simplenn(net, image);
        features = cnnResult(20).x;
    elseif (strcmpi(netName, 'vggs') ~= 0) 
        cnnResult = vl_simplenn(net, image);
        features = cnnResult(19).x;            
    elseif (strcmpi(netName, 'googlenet') ~= 0)
        net.eval({'data', image});    
        features = net.vars(net.getVarIndex('prob')).value;
    else
       error('Unrecognized network name specified.'); 
    end

    features = reshape(features, size(features, 1)*size(features, 2), size(features, 3));

end

