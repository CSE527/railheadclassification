function [numberOfImages imageUncroppedData] = getDataForD2(root)
    if exist(strcat(root,'/data/uncroppedimagesD2_C.mat'),'file')
        fprintf('loading saved uncroppedimagesD2_C.mat. Preprocessing...\n');
        imageUncroppedData = load(strcat(root,'/data/uncroppedimagesD2_C.mat'));       
        numberOfImages = size(imageUncroppedData.names, 2);
    else        
        fprintf('uncroppedimagesD2_C.mat does not exist! Preprocessing...\n');
        imagePath = strcat(root,'\D2\img_crop');
        imageFiles = dir(fullfile(imagePath, '*.bmp'));
        imageUncroppedData={};
        imageUncroppedData.severity= zeros(160,1);
        imageUncroppedData.imagemean = [];
        numberOfImages = size(imageFiles,1);        
        fclose('all');
        %sumOfTrainMeans = zeros(1,4);
        %sumOfTestMeans = zeros(1,4);
        for j = 1: numberOfImages
            imagename = imageFiles(j).name;
            severity = getSeverityFromFilename(imagename);
            image = imread(fullfile(imagePath, imagename));        
            image = im2single(image);
            [height, width, m] = size(image);                           
            currentImageMean = mean(mean(image));
            imageUncroppedData.imagemean(j) = currentImageMean;
            imageUncroppedData.names{j} = imagename;
            imageUncroppedData.severity(j) = severity;                                                          
        end
        fprintf('saving uncroppedimagesD2_C.mat. Preprocessing...\n');
        save(strcat(root,'/data/uncroppedimagesD2_C.mat'), '-struct', 'imageUncroppedData') ;
    end
end