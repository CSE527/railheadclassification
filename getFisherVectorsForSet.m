function [ fisherVectors ] = getFisherVectorsForSet( featureDb, set, means, covariances, priors, normalization )
%GETFISHERVECTORS Returns the fisher vectors for the provided features,
% means, covariances and priors.
% featureDb - Must be a cell containing a 2-D array of features
%             and a cell of {startIx, endIx} indicating which
%             features come from which images.
% set       - A 1-D matrix of indices into featureDb.featureIndexes.

setSize = numel(set);

fisherVectorsCell = cell(setSize, 1);

featureCells = cell(setSize, 1);

for i = 1:setSize
    featureCells{i} = featureDb.features{set(i)};
end

parfor i = 1:setSize
    
    inputFeatures = featureCells{i}';

    fisherFeatures = vl_fisher(inputFeatures, ...
        means, ...
        covariances, ...
        priors, ...
        normalization);
    
    fisherVector = reshape(fisherFeatures, 1, numel(fisherFeatures));
    
    fisherVectorsCell{i} = fisherVector;
end

fisherVectors = cell2mat(fisherVectorsCell);

end

