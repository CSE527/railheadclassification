% Tests the extractFVSuperPixel function

fprintf('Loading images...\n');

if (exist('imdb', 'var') == 0)
    imdb = getD1Dataset('../data');
end

fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');

tic 

setIx = 1;

numImages = length(imdb.sets{setIx}.train);

trainingImages = cell(1, numImages);

% Extract the images for this training run
for i=1:numImages
    trainingImages{i} = imdb.images{imdb.sets{setIx}.train(i)};
end
trainingMean = imdb.sets{setIx}.trainingMean;

slicRegionSize = 224;
slicRegularizer = 1;

trainDatasetName = sprintf('vggm_%d_%d_train_%d', slicRegionSize, slicRegularizer, setIx);

allCnnTrainingFeatures = getSuperpixelCNNFeatures('../Data', trainDatasetName, ...
    net, trainingImages, trainingMean, slicRegionSize, slicRegularizer );

gmmClusters = 100;
gmmRepetitions = 30;
gmmCovarianceBound = 10e-3;

tic
fprintf('Computing GMM...\n');
% Compute GMM across entire set of features
[means, covariances, priors] = vl_gmm(allCnnTrainingFeatures.features, ...
    gmmClusters, ...
    'NumRepetitions', gmmRepetitions, ...
    'CovarianceBound', gmmCovarianceBound);
toc
% Compute Fisher Vectors
fprintf('Computing Fisher Vectors for Training set...\n');

trainingFisherVectors = getFisherVectors(allCnnTrainingFeatures, ...
    means, covariances, priors, 'Improved');

fprintf('Training SVM...\n');

vectors = double(trainingFisherVectors);

labels = zeros(numImages, 1, 'double');
for i=1:numImages
   labels(i) = trainingImages{i}.severity;
end

svmModel = svmtrain(labels, vectors);

toc

% Test 

numTestImages = length(imdb.sets{setIx}.test);
testLabels = zeros(numTestImages, 1, 'double');
testImages = cell(1, numTestImages);

for i=1:numTestImages
    testImage = imdb.images{imdb.sets{setIx}.test(i)};
    testLabels(i) = testImage.severity;
    testImages{i} = testImage;
end

% Get CNN features
fprintf('Extracting CNN features for Test set...\n');

testDatasetName = sprintf('vggm_%d_%d_test_%d', slicRegionSize, slicRegularizer, setIx);

allCnnTestFeatures = getSuperpixelCNNFeatures('../Data', testDatasetName, ...
    net, testImages, trainingMean, slicRegionSize, slicRegularizer );

% Get Fisher Vectors
fprintf('Computing Fisher Vectors for Test set...\n');

testingFisherVectors = getFisherVectors(allCnnTestFeatures, ...
    means, covariances, priors, 'Improved');

fprintf('Testing images...\n');

vectors = double(testingFisherVectors);
[ predicted_label, accuracy, ~] = svmpredict(testLabels, vectors, svmModel);

