Rail Head Classification
CSE527 Final Project - Fall 2015

The following is a description of the code submission for this project. 
It contains the top-level and key functions used.

%------------------------
% External Libraries
%------------------------

The code in this submission relies on the following libraries being installed and in the Matlab path:

VLFeat - used for:
- vl_slic (super pixel)
- vl_gmm (Gaussian Mixture Model)
- vl_fisher (Fisher Vector computation).

LibSVM - SVM implementation

Matconvnet - Used for implementation of Convolutional Neural Network (CNN)

%------------------------
% Summary
%------------------------

The code for this project breaks down into 3 top-level pipelines, plus
code that trains and tests different data sets.

%------------------------
% Environment
%------------------------

This code assumes the top level Code folder is in the Matlab path and
there exists a peer folder named "Data" that contains the various
datasets used herein.


% WHOLE IMAGE PIPELINE - with VGG-M

The Whole Image Pipeline code is broken down into following functions:
1. main.m function file initiates the pipeline and calls other functions from within. It accepts
   parameters like number of clusters for GMM, iterations for GMM, covariance bound for GMM, sampling
   for GMM, cost for SVM training, and kernel for svmtraining. The functions called inside main.m are 
   described below sequentially.

2. getUncroppedDateForD1.m function file loads the KLD Inc. provided D1 dataset images from the 
   data folder,extracts the severity labels of the images from the image names, and also prepares 
   the four fold sets of training and testing images from the D1 dataset. There are 939 images in 
   the dataset with 8 degrees(labels) of severity.

3. loadPretrainedModel.m function file loads the pretrained CNN (VGG-M) in this case 

4. getCNNFeaturesForD1.m function file uses the loaded pretrained model and extracts the CNN
   features for each image using the last but one layer of the CNN model. We use VLFEAT provided library
   for feature extraction. The dimension of each of the image features returned are N x 4096.

5. buildGMM.m function file generates the gmm for the input feature map which is created by concatenating
   the CNN features of the training images for the particular run of the four fold. Since GMM takes a 
   long time to compute, the GMM is performed on a subsample of the image features. To implement the same
   the sampling fraction is used from the input parameters to main function. The sampling fraction 
   extracts CNN features from a proportional number of images of each severity. To build the sampled set
   buildSampleCombinedFeatures.m function file is used.
   GMM is implemented using VLFEAT library's inbuilt vl_gmm() function

6. crossValidationRun.m function file uses the means, covariances, and priors obtained from the GMM model.
   The function then proceeds to generate fisher features of all the images in the dataset. The fisher
   features generation is implemented using VLFEAT library's inbuilt vl_fisher() function. 
   
   After generating fisher features for all images, the fisher features of training images are concatenated
   and a SVM is trained on the images' fisher features using their severity labels. This is implemented
   using LIBSVM's svmtrain() function.
 
   The generated training model is then used to predict the accuracy of the severity annotations of the
   training images. The model is also used to predict the accuracy on the testing images. This is implemented
   using LIBSVM's svmpredict() function.

7. The main function finally retrives the training and testing accuracies, calculates the confusion matrix,
   tridiagonal accuracy, and the band distance. The function for band distance calculation was provided
   by Ke.
   


% WHOLE IMAGE PIPELINE - with GOOGLENET

The Whole Image Pipeline with GOOGLENET model is similar to the above code flow.
The only difference is in the size of the image features and the sampling rate.



% WHOLE IMAGE PIPELINE - with VGG-M with Training on D1 and Testing on D2

1. main.m function file initiates the pipeline and calls other functions from within. It accepts
   parameters like number of clusters for GMM, iterations for GMM, covariance bound for GMM, sampling
   for GMM, cost for SVM training, and kernel for svmtraining. The functions called inside main.m are 
   described below sequentially.

2. getUncroppedDateForD1.m function file loads the KLD Inc. provided D1 dataset images from the 
   data folder,extracts the severity labels of the images from the image names. There are 939 images in 
   the dataset with 8 degrees(labels) of severity.

3. getDataForD2.m function file loads the whole D2 dataset images, and extracts the severity labels.
   There are 160 images in D2 dataset.

4. loadPretrainedModel.m function file loads the pretrained CNN (VGG-M) in this case 

5. getCNNFeaturesForD1.m function file uses the loaded pretrained model and extracts the CNN
   features for each image in D1 dataset using the last but one layer of the CNN model. We use 
   VLFEAT provided libraryfor feature extraction. The dimension of each of the image features 
   returned are N x 4096.

6. getCNNFeaturesForD2.m function file uses the loaded pretrained model and extracts the CNN
   features for each image in D2 dataset using the last but one layer of the CNN model. We use VLFEAT 
   provided library for feature extraction. The dimension of each of the image features returned are 
   N x 4096.

5. buildGMM.m function file generates the gmm for the input feature map which is created by concatenating
   the CNN features of all the D1 images(training). Since GMM takes a long time to compute, the GMM is 
   performed on a subsample of the image features. To implement the same the sampling fraction is used 
   from the input parameters to main function. The sampling fraction extracts CNN features from a 
   proportional number of images of each severity. To build the sampled set buildSampleCombinedFeatures.m 
   function file is used. GMM is implemented using VLFEAT library's inbuilt vl_gmm() function.

6. crossValidationRun.m function file uses the means, covariances, and priors obtained from the GMM model.
   The function then proceeds to generate fisher features of all the images in the D1 and D2 dataset. 
   The fisher features generation is implemented using VLFEAT library's inbuilt vl_fisher() function. 
   
   After generating fisher features for all images, the fisher features of (D1) images are concatenated
   and a SVM is trained on the images' fisher features using their severity labels. This is implemented
   using LIBSVM's svmtrain() function.
 
   The generated training model is then used to predict the accuracy of the severity annotations of the
   D1 images. The model is also used to predict the accuracy on all the D2 images(test images). This is 
   implemented using LIBSVM's svmpredict() function.

7. The main function finally retrives the training and testing accuracies, calculates the confusion matrix,
   tridiagonal accuracy, and the band distance. The function for band distance calculation was provided
   by Ke.

% OVERLAPPING PATCH PIPELINE - with VGG-M

MAIN SCRIPT: 
1. OverlappingPatchPipeline.m
   This file contains the pipeline that performs the 4-fold cross validation of Dataset 1 using different 
sets of parameters. 

OTHER METHODS:
1. getOverlappingPatch.m
   This function takes one original image of Dataset1 as one input. Given stride and patchSize, it will 
return patches of this image as a 3-dimension matrix. 
   It first extract CNN features for patches of every image in training set. Then GMM model is built using 
all CNN features. The next step is to compute Fisher Vector for each image in training set. Based on all 
the Fisher Vectors, a SVM model is trained. After that, CNN features for images in testing set are extracted. 
With the Fisher Vectors for images in testing set computed, evaluation of the trained SVM is performed. 

2. getD1DatasetByFilename.m
   This function does the prprocessing on Dataset 1. It takes the file root as the input. The output is a 
structure containing severity and filenames for each image, the mean of all images and 4 training sets as 
well as 4 testing sets. 

% WHOLE IMAGE PIPELINE - with VGG-M - Training on Dataset 1 and Testing on Dataset 3

MAIN SCRIPT:

1. trainD1testD3v2.m
    This file contains the top level script code that was used to produce the final results 
for training D1 and testing D3. This implementation uses parallel processing in Matlab
to speed up computation as much as possible. This implementation also does no sampling
of any kind - it uses all CNN features and Fisher Vectors of the training set.
Approximate runtime of this code on a Macbook Pro with quad-core i7 and 16GB Ram is 4 hours.

2. trainD1testD3.m 
   This file contains the pipeline that performs training SVM on Dataset 1 and evaluation on Dataset 3 
using the optimal set of parameters generated from previous experiments. 
   CNN features of every image in Dataset 1 and Dataset 3 are extracted. The Fisher Vectors are computed 
based on the extracted CNN features for both dataasets. Then GMM model is built using 10% of all extracted 
CNN features of Dataset 1. SVM model is trained using Fisher Vectors of Dataset 1. Then evaluation is 
performed using Fisher Vectors of both Dataset 1 and Dataset 3. 

OTHER METHODS:
1. getD1DatasetByFilename.m 
   As is described above, this function does the preprocessing on Dataset 1.  

2. getD3DatasetByFilename.m
   Similarly, this function does the propressing on Dataset 3. The structures of the input and output 
of this function is the same as getD1DatasetByFilename. 

3. getWholeImageCNNFeaturesByFilename.m
    Extracts the CNN features of the entire image set passed to it. This function saves the results
to a specifically named file. If that file is present when the function is run, the contents of 
the file are loaded and returned rather than computing the results again.

4. getFisherVectorsForSet.m
    Generates the Fisher Vectors for the indicated set of CNN features and GMM data.
This function uses Matlab's parfor construct to use concurrent processing to speed up
processing.

5. computeConfusionResult.m
    Constructs a confusion matrix based on a set of actual and predicted labels. 
Computes accuracy, tri-diagonal accuracy, and band distance.

% SUPER-PIXEL PIPELINE WITH VGG-M and GoogleNet
The super-pixel pipeline is broken down into the following functions:

1) SuperpixelPipeline/crossValidateD1Batch.m - This file contains a script that
performs the cross validation of D1 using various parameters.

2) SuperpixelPipeline/crossValidateD1.m - Functiont that coordinates the 4-fold cross validation. 
Loads the image database, loads the appropriate CNN

3) SuperpixelPipeline/getSuperpixelCNNFeaturesByFilename.m - Loads (or computes if data file is not present)
the super pixels and CNN Features for the indicated image set in the image database.

4) SuperpixelPipeline/crossValidate.m - Extracts the features for the indicated training data set, and loads (or computes)
the GMM for the training data set. Computes Fisher Vectors, trains the SVM, extracts Fisher Vectors for
the testing set, and predicts training accuracy, testing accuracy and confusion matrix results.

% WHOLE IMAGE 4-FOLD CROSS VALIDATION OF D3

1) crossValidateD3.m - A Matlab script that coordinates the 4-fold cross validation of the D3 dataset.
It loads the VGG-M CNN, loads the image database, gets CNN features for the entire D3 dataset, then calls trainAndTest
in a loop to train and test each of the 4 folds.

2) getD3DatasetByFilename.m - Loads the D3 dataset into an in-memory structure, grouping images into separate
training and testing sets. It does NOT load the actual image data into memory but rather just creates
structures that include the image filename and severity.

3) trainAndTest.m - Extracts the features for the training set, computes the GMM, computes the
Fisher Vectors, trains the SVM, extracts the features for the test set, then predicts training accuracy, 
testing accuracy, and confusion matrix results.
 