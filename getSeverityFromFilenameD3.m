function [ severity ] = getSeverityFromFilenameD3( filename )
%GETSEVERITYFROMFILENAME Parses the filename and extracts the severity
%encoded within it.
% This assumes that severity is in the second position of
% <first>_<second>_<third>.<fileextension>
% Returns -1 if the severity cannot be parsed (i.e. if filename is
% malformed).

underscoreIx = find(filename == '_', 2);

if (isempty(underscoreIx) || (size(underscoreIx, 2) < 2))
   severity = -1;
   return;
end

[severity, status] = str2num(filename(underscoreIx(1)+1:underscoreIx(2)-1));

if (status == 0)
    severity = -1;
    return;
end

% D3 images are encoced 0-7, but we need 1-8
severity = severity + 1;


end

