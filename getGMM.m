function [ means, covariances, priors ] = getGMM( dataRoot, dataName, ...
    features, clusters, repetitions, covarianceBound )
%GETGMM Computes/caches the GMM for the given parameters
    
    filename = sprintf('gmm_%s_%d_%d_%s.mat', dataName, clusters, ...
        repetitions, getNameForFloat(covarianceBound));
    
    filename = fullfile(dataRoot, filename);
    
    if (exist(filename, 'file'))
        data = load(filename);
        means = data.means;
        covariances = data.covariances;
        priors = data.priors;
    else
        data = {};
        
        [ means, covariances, priors ] = vl_gmm(features, ...
            clusters, ...
            'NumRepetitions', repetitions, ...
            'CovarianceBound', covarianceBound);
            
        data.means = means;
        data.covariances = covariances;
        data.priors = priors;
        
        save(filename, '-struct', 'data', '-v7.3');
    end
end

