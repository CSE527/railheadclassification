% Sampling function to sample images
function combinedFeatures = buildSampleCombinedFeatures(cnnFeatures, severityLabels, samplesize, distinctseverity)
    combinedFeatures = [];
    perLabelSize = ceil (samplesize/size(distinctseverity,1));
    remaining = ones(size(distinctseverity,1), 1);
    remaining = remaining*double(perLabelSize);
    for i = 1:size(cnnFeatures,2)
        severity = severityLabels(i);
        if (remaining(severity) <= 0)
            continue;
        end
        remaining(severity) = remaining(severity) - 1;
        combinedFeatures = [combinedFeatures;cnnFeatures{i}];
        if (sum(remaining) == 0)
            break;
        end
    end
end