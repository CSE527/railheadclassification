function [ predicted_labels_train, accuracy_train, prob_estimates_train, ...
           predicted_labels_test, accuracy_test, prob_estimates_test, ...
           confusionMatrix] = crossValidate( dataRoot, dataName, imdb, setIx, cnnFeatureDb, ...
           clusters, repetitions, covarianceBound, svmCost, kernelType )
%CROSSVALIDATE Runs a single cross validation on dataset D1 

    trainingSet = imdb.sets{setIx}.train;
    testingSet = imdb.sets{setIx}.test;
   
    numTrainingImages = size(trainingSet, 2);
    numTestingImages = size(testingSet, 2);
    
    % Build the GMM 
  
    % Calculate then size of the training feature matrix
    numTrainingFeatures = 0;
    for i=1:numTrainingImages
        indexes = cnnFeatureDb.featureIndexes{trainingSet(i)};        
       
        numTrainingFeatures = numTrainingFeatures + (indexes.endIx - indexes.startIx + 1);       
    end
    
    % Build the training feature matrix
    
    features = zeros(size(cnnFeatureDb.features, 1), numTrainingFeatures, 'single');
    
    featureIx = 1;
    
    for i=1:numTrainingImages
        
        indexes = cnnFeatureDb.featureIndexes{trainingSet(i)};
        
        endIx = featureIx + (indexes.endIx - indexes.startIx);        
        
        features(:, featureIx:endIx) = cnnFeatureDb.features(:, indexes.startIx:indexes.endIx);
        
        featureIx = endIx + 1;
        
    end
    
    fprintf('Computing GMM for training set %d...\n', setIx);

    gmmName = sprintf('%s_%d', dataName, setIx);
    
    [means, covariances, priors] = getGMM(dataRoot, gmmName, features, ...
        clusters, repetitions, covarianceBound);
    
    clearvars features;
    
    fprintf('Computing Fisher Vectors...\n');    
    
    trainingFisherVectors = getFisherVectorsForSet(cnnFeatureDb, trainingSet, ...
        means, covariances, priors, 'Improved');
    
    trainingFisherVectors = double(trainingFisherVectors);
    
    fprintf('Training SVM...\n');
    
    trainingLabels = zeros(numTrainingImages, 1, 'double');
    for i=1:numTrainingImages
        trainingLabels(i) = imdb.images{trainingSet(i)}.severity;
    end

    % Train the SVM with a linear kernel.
    svmModel = svmtrain(trainingLabels, trainingFisherVectors, sprintf('-t %d -c %d', kernelType, svmCost));
    
    % Get training Accuracy
    fprintf('Computing training accuracy...\n');
    [ predicted_labels_train, accuracy_train, prob_estimates_train ] = svmpredict(trainingLabels, ...
        trainingFisherVectors, svmModel);
    
    % Test
    fprintf('Testing. Computing Fisher Vectors...\n');
    testingFisherVectors = getFisherVectorsForSet(cnnFeatureDb, testingSet, ...
        means, covariances, priors, 'Improved');  
    
    testLabels = zeros(numTestingImages, 1, 'double');
    for i=1:numTestingImages
       testLabels(i) = imdb.images{testingSet(i)}.severity; 
    end
    
    testingFisherVectors = double(testingFisherVectors);    
    
    fprintf('Testing with SVM...\n');
    [ predicted_labels_test, accuracy_test, prob_estimates_test] = svmpredict(testLabels, ...
        testingFisherVectors, svmModel);
    
    confusionMatrix = computeConfusionResult(predicted_labels_test, testLabels, 8);
    
    fprintf('*** Training Accuracy = %f%%, Testing Accuracy = %f%%\n', accuracy_train, accuracy_test);
end

