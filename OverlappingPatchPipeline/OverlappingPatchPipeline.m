% Overlapping Patch Pipeline
clear;clc;

% run C:\Users\Heyi\Downloads\matconvnet-1.0-beta16/matlab/vl_setupnn
% run('C:\Users\Heyi\Downloads\vlfeat-0.9.20/toolbox/vl_setup')
% addpath('C:\Users\Heyi\Downloads\libsvm-3.20\libsvm-3.20\matlab');

fprintf('Performing 4-fold cross validation on D1 using overlapping patches.\n');

patchSize = 224;
% parameter to be tuned
stride = 100;
% stride = 50;
gmmClusters = 45;
gmmRepetitions = 20;
gmmCovarianceBound = 1e-6;
% gmmClusters = 30;  
% gmmRepetitions = 10;
% gmmCovarianceBound = 1e-4;

% load the Convolutional Neural Network
netName = 'vgg-m';
fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');
vggNet = net;
for i = 1: numel(net.layers)
    if strcmp(net.layers{1,i}.type, 'conv')
        vggNet.layers{1,i} = rmfield(net.layers{1,i}, 'weights');
        vggNet.layers{1,i}.filters = net.layers{1,i}.weights{1,1};
        vggNet.layers{1,i}.biases = net.layers{1,i}.weights{1,2};
    end;
end;

% Dataset 1 preprocessing
fprintf('Loading images...\n');
dataRoot = '../Data';
imdb = getD1DatasetByFilename(dataRoot);

% 4-fold cross validation
for i = 1:1:4
    tic
    fprintf('Running cross validation %d of 4\n',i);
    featureFilename = sprintf('D1_cnnFeatures_train%d_%s_%d.mat', i, netName, stride);
    featureFilename = fullfile(dataRoot, featureFilename);
    
    if (exist(featureFilename, 'file') == 2)
        features = load(featureFilename);
        featureIndex = features.featureIndexes;
        cnnFeature = features.features;
        severity = features.severity;
    else
        featureIndex = zeros(size(imdb.sets{i}.train,2),2);
        severity = zeros(1,size(imdb.sets{i}.train,2));
        for j = 1:1:size(imdb.sets{i}.train,2)
            % load original image
            image = imdb.images{imdb.sets{i}.train(j)}.data;
            image = image - imdb.sets{i}.trainingMean;
            severity(j) = imdb.images{imdb.sets{i}.train(j)}.severity;
            [height, width] = size(image);
            % Step 1
            % extract overlapping patches
            fprintf('Extracting patches for training images %d of %d\n',j,size(imdb.sets{i}.train,2));
            patch = getOverlappingPatch(image, ...
                patchSize, ...
                height, ...
                width, ...
                stride);
            % Step 2
            % extract Convolutional Neural Network features
            fprintf('Extracting CNN features for training images %d of %d\n',j,size(imdb.sets{i}.train,2));
            numberOfPatches = size(patch, 3);
            for p = 1:1:numberOfPatches
                input = patch(:,:,p);
                input = im2single(input);
                input = cat(3, input, input, input);
                res = vl_simplenn(vggNet, input);
                imageFeature = res(20).x;
                imageFeature = reshape(imageFeature,1,[]);
                if (j == 1) && (p == 1)
                    cnnFeature = imageFeature;
                else
                    cnnFeature = cat(1, cnnFeature, imageFeature);
                end;
            end;
            if j == 1
                featureIndex(j,1) = 1;
                featureIndex(j,2) = numberOfPatches;
            else
                featureIndex(j,1) = featureIndex(j-1,2)+1;
                featureIndex(j,2) = featureIndex(j,1)+numberOfPatches-1;
            end;
        end;
        % save extracted CNN features
        features = {};
        features.featureIndexes = featureIndex;
        features.features = cnnFeature;
        features.severity = severity;
        save(featureFilename, '-struct', 'features', '-v7.3');
    end;
    
    % Step 3
    % compute Gaussian Mixture Model across entire set of features
    modelFilename = sprintf('D1_gmm_%d_%s_%d_%d_%d_%s.mat', i, netName, stride, gmmClusters, ...
        gmmRepetitions, num2str(gmmCovarianceBound));
    modelFilename = fullfile(dataRoot, modelFilename);
    cnnFeature = cnnFeature';
    if (exist(modelFilename, 'file'))
        data = load(modelFilename);
        means = data.means;
        covariances = data.covariances;
        priors = data.priors;
    else
        fprintf('Computing Gaussian Mixture Model...\n');
        [means, covariances, priors] = vl_gmm(cnnFeature, ...
            gmmClusters, ...
            'NumRepetitions', gmmRepetitions, ...
            'CovarianceBound', gmmCovarianceBound);
        fprintf('Gaussian Mixture Model completed...\n');
        % save GMM
        data = {};
        data.means = means;
        data.covariances = covariances;
        data.priors = priors;
        
        save(modelFilename, '-struct', 'data', '-v7.3');
    end;
    
    % Step 4
    % compute Fisher Vector for each image
    for j = 1:1:size(imdb.sets{i}.train,2)
        inputFeature = cnnFeature(:,featureIndex(j,1):featureIndex(j,2));
        fprintf('Computing Fisher Vectors for training images %d of %d\n', j, size(imdb.sets{i}.train,2));
        fisherFeature = vl_fisher(inputFeature, ...
            means, ...
            covariances, ...
            priors, ...
            'Improved');
        fisherVector = reshape(fisherFeature, 1, numel(fisherFeature));
        if j == 1
            fisherVectors = fisherVector;
        else
            fisherVectors = cat(1, fisherVectors, fisherVector);
        end;
    end;
    % Step 5
    % train SVM with a linear kernel
    fprintf('Training SVM...\n');
    vectorsTrain = double(fisherVectors);
    labelsTrain = severity;
    labelsTrain = reshape(labelsTrain, numel(labelsTrain), 1);
    labelsTrain = double(labelsTrain);
    svmModel = svmtrain(labelsTrain, vectorsTrain, '-t 0 -c 2');
    fprintf('Training SVM completed...\n');
    
    clearvars featureIndex severity cnnFeature fisherVector fisherVectors;
    
    featureFilename = sprintf('D1_cnnFeatures_test%d_%s_%d.mat', i, netName, stride);
    featureFilename = fullfile(dataRoot, featureFilename);
    
    if (exist(featureFilename, 'file') == 2)
        features = load(featureFilename);
        featureIndex = features.featureIndexes;
        cnnFeature = features.features;
        severity = features.severity;
    else
        featureIndex = zeros(size(imdb.sets{i}.test,2),2);
        severity = zeros(1,size(imdb.sets{i}.test,2));
        for j = 1:1:size(imdb.sets{i}.test,2)
            % load original image
            image = imdb.images{imdb.sets{i}.test(j)}.data;
            image = image - imdb.sets{i}.trainingMean;
            severity(j) = imdb.images{imdb.sets{i}.test(j)}.severity;
            [height, width] = size(image);
            % extract overlapping patches
            fprintf('Extracting patches for testing images %d of %d\n',j,size(imdb.sets{i}.test,2));
            patch = getOverlappingPatch(image, ...
                patchSize, ...
                height, ...
                width, ...
                stride);
            % extract Convolutional Neural Network features
            fprintf('Extracting CNN features for testing images %d of %d\n',j,size(imdb.sets{i}.test,2));
            numberOfPatches = size(patch, 3);
            for p = 1:1:numberOfPatches
                input = patch(:,:,p);
                input = im2single(input);
                input = cat(3, input, input, input);
                res = vl_simplenn(vggNet, input);
                imageFeature = res(20).x;
                imageFeature = reshape(imageFeature,1,[]);
                if (j == 1) && (p == 1)
                    cnnFeature = imageFeature;
                else
                    cnnFeature = cat(1, cnnFeature, imageFeature);
                end;
            end;
            if j == 1
                featureIndex(j,1) = 1;
                featureIndex(j,2) = numberOfPatches;
            else
                featureIndex(j,1) = featureIndex(j-1,2)+1;
                featureIndex(j,2) = featureIndex(j,1)+numberOfPatches-1;
            end;
        end;
        % save extracted CNN features
        features = {};
        features.featureIndexes = featureIndex;
        features.features = cnnFeature;
        features.severity = severity;
        save(featureFilename, '-struct', 'features', '-v7.3');
    end;
    cnnFeature = cnnFeature';
    % compute Fisher Vector for each image
    for j = 1:1:size(imdb.sets{i}.test,2)
        inputFeature = cnnFeature(:,featureIndex(j,1):featureIndex(j,2));
        fprintf('Computing Fisher Vectors for testing images %d of %d\n', j, size(imdb.sets{i}.test,2));
        fisherFeature = vl_fisher(inputFeature, ...
            means, ...
            covariances, ...
            priors, ...
            'Improved');
        fisherVector = reshape(fisherFeature, 1, numel(fisherFeature));
        if j == 1
            fisherVectors = fisherVector;
        else
            fisherVectors = cat(1, fisherVectors, fisherVector);
        end;
    end;
    
    % Step 6
    % evaluate accuracy on training set and testing set
    vectorsTest = double(fisherVectors);
    labelsTest = severity;
    labelsTest = reshape(labelsTest, numel(labelsTest), 1);
    labelsTest = double(labelsTest);
    
    fprintf('Performing evaluation...\n');
    [predicted_labels_train, accuracy_train, prob_estimates_train] = svmpredict(labelsTrain, vectorsTrain, svmModel);
    [predicted_labels_test, accuracy_test, prob_estimates_test] = svmpredict(labelsTest, vectorsTest, svmModel);
    confusionResult = computeConfusionResult(predicted_labels_test, labelsTest, 8);
    % save classification result
    resultFilename = sprintf('D1_result_%d_%s_%d_%d_%d_%s.mat', i, netName, stride, ...
        gmmClusters, gmmRepetitions, num2str(gmmCovarianceBound));
    resultFilename = fullfile(dataRoot, resultFilename);
    
    results = {};
    results.predicted_labels_train = predicted_labels_train;
    results.accuracy_train = accuracy_train;
    results.prob_estimates_train = prob_estimates_train;
    results.predicted_labels_test = predicted_labels_test;
    results.accuracy_test = accuracy_test;
    results.prob_estimates_test = prob_estimates_test;
    results.confusionResult = confusionResult;
    save(resultFilename, '-struct', 'results', '-v7.3');
    toc
end;
