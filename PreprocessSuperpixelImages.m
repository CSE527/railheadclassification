% Preprocess all images in datasets D1, D2, and D3 by extracting 
% superpixels and saving the data as a single "database" MAT file for each 
% dataaset.

% Tunable parameters
slicRegionSize = 75;
slicRegularizer = .5;
slicMinRegionSize = 50;

