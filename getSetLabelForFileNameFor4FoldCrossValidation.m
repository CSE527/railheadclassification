% Pass function parameters consisting of arrays of filenames for the 4 fold
% cross validation, along with the current file name
% returns set label [a b c d] where a,b,c,d can have values (1,2)
% 1 implies training
% 2 implies testing
function labels = getSetLabelForFileNameFor4FoldCrossValidation(train1Array, test1Array,...
                                                       train2Array, test2Array,...
                                                       train3Array, test3Array,...
                                                       train4Array, test4Array, ...
                                                       currentFileName)
    label1 = getSetLabel(train1Array, test1Array, currentFileName);
    label2 = getSetLabel(train2Array, test2Array, currentFileName);
    label3 = getSetLabel(train3Array, test3Array, currentFileName);
    label4 = getSetLabel(train4Array, test4Array, currentFileName);  
    labels = [label1, label2, label3, label4];
end

function label = getSetLabel(trainArray, testArray, fileName)
    trainsize = size(trainArray,2);    
    label = -1;
    for i = 1:trainsize
        if strcmp(fileName,trainArray(:,i)) == 1
            label = 1;
            break;
        end
    end
    if label == -1
        testsize = size(testArray,2);
        for i = 1:testsize
            if strcmp(fileName,testArray(:,i)) == 1
                label = 2;
                break;
            end
        end
    end
end
