clear;clc;
% train SVM on Dataset 1
% test on Dataset 3

dataRoot = '../Data';
% optimal parameters 
netName = 'vgg-m';
gmmClusters = 45;
gmmRepetitions = 20;
gmmCovarianceBound = 1e-6;

tic

% load the Convolutional Neural Network
fprintf('Loading VGG-M...\n');
net = load('../Data/CNN/imagenet-vgg-m.mat');
vggNet = net;
for i = 1: numel(net.layers)
    if strcmp(net.layers{1,i}.type, 'conv')
        vggNet.layers{1,i} = rmfield(net.layers{1,i}, 'weights');
        vggNet.layers{1,i}.filters = net.layers{1,i}.weights{1,1};
        vggNet.layers{1,i}.biases = net.layers{1,i}.weights{1,2};
    end;
end;

% load or extract CNN features for D1
featureFilename = sprintf('D1_cnnFeatures_%s.mat', netName);
featureFilename = fullfile(dataRoot, featureFilename);
imdb1 = getD1DatasetByFilename(dataRoot);
imgMean = imdb1.imageMean;
if (exist(featureFilename, 'file') == 2)
    features = load(featureFilename);
    cnnFeature = features.features;
    severity = features.severity;
    featureIndex = features.featureIndex;
    numberOfImages = size(severity,2);
else
    fprintf('Loading Dataset1 images...\n');
    numberOfImages = size(imdb1.images,2);
    featureIndex = zeros(numberOfImages,2);
    severity = zeros(1,numberOfImages);
    imagePath = fullfile(dataRoot, 'KLD_TF/Images');
    for i = 1:1:numberOfImages
        imgName = imdb1.images{2}.filename;
        img = imread(fullfile(imagePath, imgName)); 
        severity(i) = imdb1.images{i}.severity;
        img = single(img);
        img = img - imgMean;
        img = cat(3, img, img, img);
        fprintf('Extracting CNN features for D1 images %d of %d\n',i,numberOfImages);
        res = vl_simplenn(vggNet, img);
        imageFeature = res(20).x;
        imageFeature = reshape(imageFeature,size(imageFeature,1)*size(imageFeature,2),size(imageFeature,3));
        if i == 1
            cnnFeature = imageFeature;
            featureIndex(i,1) = 1;
            featureIndex(i,2) = size(imageFeature,1);
        else
            cnnFeature = cat(1, cnnFeature, imageFeature);
            featureIndex(i,1) = featureIndex(i-1,2)+1;
            featureIndex(i,2) = featureIndex(i,1)+size(imageFeature,1)-1;
        end;
    end;
    % save extracted CNN features
    features = {};
    features.features = cnnFeature;
    features.severity = severity;
    features.featureIndex = featureIndex;
    save(featureFilename, '-struct', 'features', '-v7.3');
end;
clearvars features;

% compute Gaussian Mixture Model across sampled set of features from D1
% sampling rate  = 10%
modelFilename = sprintf('D1_gmm_%s_%d_%d_%s.mat', netName, gmmClusters, ...
    gmmRepetitions, num2str(gmmCovarianceBound));
modelFilename = fullfile(dataRoot, modelFilename);
if (exist(modelFilename, 'file'))
    data = load(modelFilename);
    means = data.means;
    covariances = data.covariances;
    priors = data.priors;
else
    fprintf('Computing Gaussian Mixture Model...\n');
    for i = 1:1:numberOfImages
        count = (featureIndex(i,2) - featureIndex(i,1)) / 10;
        if i == 1
            newCnnFeature = cnnFeature(featureIndex(i,1):featureIndex(i,1)+count,:);
        else
            newCnnFeature = cat(1, newCnnFeature, cnnFeature(featureIndex(i,1):featureIndex(i,1)+count,:));
        end;
    end;
    [means, covariances, priors] = vl_gmm(newCnnFeature', ...
        gmmClusters, ...
        'NumRepetitions', gmmRepetitions, ...
        'CovarianceBound', gmmCovarianceBound);
    fprintf('Gaussian Mixture Model completed...\n');
    % save GMM
    data = {};
    data.means = means;
    data.covariances = covariances;
    data.priors = priors;
    save(modelFilename, '-struct', 'data', '-v7.3');
end;

% compute Fisher Vector for each image in D1
for i = 1:1:numberOfImages
    inputFeature = cnnFeature(featureIndex(i,1):featureIndex(i,2),:);
    fprintf('Computing Fisher Vectors for D1 images %d of %d\n', i, numberOfImages);
    fisherFeature = vl_fisher(inputFeature', ...
        means, ...
        covariances, ...
        priors, ...
        'Improved');
    fisherVector = reshape(fisherFeature, 1, numel(fisherFeature));
    if i == 1
        fisherVectors = fisherVector;
    else
        fisherVectors = cat(1, fisherVectors, fisherVector);
    end;
end;
clearvars cnnFeature;

% train SVM with a linear kernel
fprintf('Training SVM...\n');
vectorsTrain = double(fisherVectors);
labelsTrain = severity;
clearvars severity inputFeature fisherFeature fisherVector fisherVectors;
labelsTrain = reshape(labelsTrain, numel(labelsTrain), 1);
labelsTrain = double(labelsTrain);
svmModel = svmtrain(labelsTrain, vectorsTrain, '-t 0 -c 2');
fprintf('Training SVM completed...\n');

% load or extract CNN features for D3
featureFilename = sprintf('D3_cnnFeatures_%s.mat', netName);
featureFilename = fullfile(dataRoot, featureFilename);
imdb2 = getD3DatasetByFilename(dataRoot);
if (exist(featureFilename, 'file') == 2)
    features = load(featureFilename);
    cnnFeature = features.features;
    severity = features.severity;
    featureIndex = features.featureIndex;
else
    fprintf('Loading Dataset3 images...\n');
    numberOfImages = size(imdb2.images,2);
    featureIndex = zeros(numberOfImages,2);
    severity = zeros(1,numberOfImages);
    imagePath = fullfile(dataRoot, 'D3/Images');
    for i = 1:1:numberOfImages
        imgName = imdb2.images{2}.filename;
        img = imread(fullfile(imagePath, imgName)); 
        severity(i) = imdb2.images{i}.severity;
        img = single(img);
        d = min(size(img, 1), size(img, 2));
        r = 224 / d;
        if (d < 224)
            img = imresize(img, [size(img, 1) * r, size(img, 2) * r]);
        end;
        img = img - imgMean;
        img = cat(3, img, img, img);
        fprintf('Extracting CNN features for D3 images %d of %d\n',i,numberOfImages);
        res = vl_simplenn(vggNet, img);
        imageFeature = res(20).x;
        imageFeature = reshape(imageFeature,size(imageFeature,1)*size(imageFeature,2),size(imageFeature,3));
        if i == 1
            cnnFeature = imageFeature;
            featureIndex(i,1) = 1;
            featureIndex(i,2) = size(imageFeature,1);
        else
            cnnFeature = cat(1, cnnFeature, imageFeature);
            featureIndex(i,1) = featureIndex(i-1,2)+1;
            featureIndex(i,2) = featureIndex(i,1)+size(imageFeature,1)-1;
        end;
    end;
    % save extracted CNN features
    features = {};
    features.features = cnnFeature;
    features.severity = severity;
    features.featureIndex = featureIndex;
    save(featureFilename, '-struct', 'features', '-v7.3');
end;
% compute Fisher Vector for each image in D3
for i = 1:1:numberOfImages
    inputFeature = cnnFeature(featureIndex(i,1):featureIndex(i,2),:);
    fprintf('Computing Fisher Vectors for D3 images %d of %d\n', i, numberOfImages);
    fisherFeature = vl_fisher(inputFeature', ...
        means, ...
        covariances, ...
        priors, ...
        'Improved');
    fisherVector = reshape(fisherFeature, 1, numel(fisherFeature));
    if i == 1
        fisherVectors = fisherVector;
    else
        fisherVectors = cat(1, fisherVectors, fisherVector);
    end;
end;

% evaluate accuracy on training set and testing set
vectorsTest = double(fisherVectors);
labelsTest = severity;
labelsTest = reshape(labelsTest, numel(labelsTest), 1);
labelsTest = double(labelsTest);

clearvars severity cnnFeature fisherVector fisherVectors;

fprintf('Performing evaluation...\n');
[predicted_labels_train, accuracy_train, prob_estimates_train] = svmpredict(labelsTrain, vectorsTrain, svmModel);
[predicted_labels_test, accuracy_test, prob_estimates_test] = svmpredict(labelsTest, vectorsTest, svmModel);
confusionResult = computeConfusionResult(predicted_labels_test, labelsTest, 8);
% save classification result
resultFilename = sprintf('Result_trainD1_testD3_%s_%d_%d_%s.mat', netName, ...
    gmmClusters, gmmRepetitions, num2str(gmmCovarianceBound));
resultFilename = fullfile(dataRoot, resultFilename);
    
results = {};
results.predicted_labels_train = predicted_labels_train;
results.accuracy_train = accuracy_train;
results.prob_estimates_train = prob_estimates_train;
results.predicted_labels_test = predicted_labels_test;
results.accuracy_test = accuracy_test;
results.prob_estimates_test = prob_estimates_test;
results.confusionResult = confusionResult;
save(resultFilename, '-struct', 'results', '-v7.3');

toc

