function [ results ] = crossValidateD1(netType, ...
    regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType)

fprintf('Performing 4-fold cross validation on D1 using super pixel.\n');

tic

if (strcmpi(netType, 'vggm') ~= 0)
    fprintf('Loading VGG-M...\n');
    net = load('../../Data/CNN/imagenet-vgg-m.mat');
elseif (strcmpi(netType, 'vggs') ~= 0)
    fprintf('Loading VGG-S...\n');
    net = load('../../Data/CNN/imagenet-vgg-s.mat');
elseif (strcmpi(netType, 'googlenet') ~= 0)
    fprintf('Loading GoogleNet...\n');
    googleNetData = load('../../Data/CNN/imagenet-googlenet-dag.mat');
    net = dagnn.DagNN.loadobj(googleNetData);
else
    error('Unrecognized network name specified.');
end

fprintf('Loading images...\n');
imdb = getD1DatasetByFilename('../../data');

% Get the features for the entire dataset
featureDb = getSuperpixelCNNFeaturesByFilename('../../Data', '../../Data/KLD_TF/Images', ...
    netType, imdb, imdb.imageMean, net, regionSize, regularizer);

results = {};
results.results = cell(1, 4);

for setIx=1:4
    
    dataName = sprintf('sp_%s_r%d_r%s_D1', netType, regionSize, getNameForFloat(regularizer));
    
    [ predicted_labels_train, accuracy_train, prob_estimates_train, ...
      predicted_labels_test, accuracy_test, prob_estimates_test, confusionResults ] = ...
      crossValidate('../../Data', dataName, imdb, setIx, featureDb, ...
        clusters, repetitions, covarianceBound, ...
        svmCost, kernelType);
  
    results.results{setIx}.predicted_labels_train = predicted_labels_train;
    results.results{setIx}.accuracy_train = accuracy_train;
    results.results{setIx}.prob_estimates_train = prob_estimates_train;
    results.results{setIx}.predicted_labels_test = predicted_labels_test;
    results.results{setIx}.accuracy_test = accuracy_test;
    results.results{setIx}.prob_estimates_test = prob_estimates_test;
    results.results{setIx}.confusionResults = confusionResults;
    
end

saveResults('../../Data', results, dataName, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

toc

end