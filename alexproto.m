% Prototype of connecting AlexNet to Fisher Vector to SVM

tic

fprintf('Loading Alexnet...\n');

net = load('../Data/CNN/imagenet-caffe-alex.mat');

% Load images
imageFiles = {
    '1_1_s.bmp', ...
    '2_1_s.bmp', ...
    '3_1_s.bmp', ...
    '4_1_s.bmp', ...
    '5_1_s.bmp', ...
    '6_1_s.bmp'};

% Tunable parameters
numClusters = 45;  
numRepetitions = 20;
covarianceBound = 10e-6;
fisherNormalization = 'Improved'; % Normalized, SquareRoot, Improved, Fast

% Load every image into memory
imdb.labels = [1;2;3;4;5;6];
images = cell(length(imageFiles), 1);
for i = 1:length(images)
    images{i} = imread(fullfile('../Data/KLD_TF/Images', imageFiles{i}));    
end

% Normalize image sizes
imagey = 1000000;
imagex = 1000000;
for i = 1:length(images)
   im = images{i};
   imagey = min(imagey, size(im, 1));
   imagex = min(imagex, size(im, 2));
end

imdb.images = cell(length(images), 1);
imdb.labels = [1;2;3;4;5;6];
% imdb.images = zeros(length(images), imagey, imagex, 3, 'single');

for i = 1:length(images)
    im = images{i};
   
    image = cropImage(im, imagey, imagex);   
    image = im2single(image);
    
    %imdb.images(i, :) = reshape(image, [imagey imagex 3]);   
    imdb.images{i} = image;
end

% Compute mean and subtract from each image

%imageMean = mean(imdb.images);
%imdb.images = imdb.images - imageMean;

% The resulting Fisher feature size will be 
% (input image m / 224) *
% (input image n / 224) *
% numClusters * 2
%fisherFeatureSize = numClusters*2*floor(imagey/224)*floor(imagex/224);
fisherFeatureSize = 27900;
fisherFeatures = zeros(length(imageFiles), fisherFeatureSize, 'double');

for i = 1: length(imdb.images)

    % Load and Prepare image
    %rawImage = imread(fullfile('../Data/KLD_TF/Images', imageFiles{i}));
    %image = cropImage(rawImage, imagey, imagex);   
    %image = im2single(image);
    image = imdb.images{i};
   
    % Extract the features from the CNN
    netres = vl_simplenn(net, image);
    imageFeatures = netres(20).x;
    
    cnnFeatures = reshape(imageFeatures, ...
        [size(imageFeatures, 1)*size(imageFeatures, 2), 4096]);

    % Compute GMM

    fprintf('Computing GMM and Fisher Vectors...\n');

    features = extractFisherVector(cnnFeatures, ...
        numClusters, numRepetitions, covarianceBound, fisherNormalization); 

    fisherFeatures(i, :) = reshape(features, [1 fisherFeatureSize]);

end

% Train SVM

fprintf('Training SVM...\n');

svmModel = svmtrain(imdb.labels, fisherFeatures);


toc