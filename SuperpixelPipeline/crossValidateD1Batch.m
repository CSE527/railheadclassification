addpath('..');

svmCost = 2;
kernelType = 0; % Linear

% Superpixel 100/0.01
regionSize = 100;
regularizer = 0.01;

% VGG-M
netType = 'vggm';
% netType = 'googlenet';

% Need to try (45, 20, 1e-6) and (30, 10, 1e-4)

clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;

[ results_100_d01_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 30;
repetitions = 10;
covarianceBound = 1e-4;

[ results_100_d01_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

regionSize = 200;
regularizer = 0.01;

clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;

[ results_100_d01_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 30;
repetitions = 10;
covarianceBound = 1e-4;

[ results_100_d01_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 20;
repetitions = 20;
covarianceBound = 1e-4;

[ results_200_d1_20_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 60;
repetitions = 20;
covarianceBound = 1e-4;

[ results_200_d1_60_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

% Superpixel 200/0.1
regionSize = 200;
regularizer = 0.1;

% Need to try (45, 20, 1e-6) and (30, 10, 1e-4)

% clusters = 45;
% repetitions = 20;
% covarianceBound = 1e-6;
% 
% [ results_200_d1_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 30;
repetitions = 10;
covarianceBound = 1e-4;

% [ results_200_d1_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);

% GoogleNet
netType = 'googlenet';

[ results_goog_200_d1_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;

[ results_goog_200_d1_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

regionSize = 100;
regularizer = 0.01;

clusters = 30;
repetitions = 10;
covarianceBound = 1e-4;

[ results_goog_100_d01_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);

clusters = 45;
repetitions = 20;
covarianceBound = 1e-6;

[ results_goog_100_d01_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
    clusters, repetitions, covarianceBound, svmCost, kernelType);


% VGG-S
% netType = 'vggs';
% 
% regionSize = 200;
% regularizer = 0.1;
% 
% clusters = 30;
% repetitions = 10;
% covarianceBound = 1e-4;
% 
% [ results_vggs_200_d1_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);
% 
% clusters = 45;
% repetitions = 20;
% covarianceBound = 1e-6;
% 
% [ results_vggs_200_d1_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);
% 
% regionSize = 100;
% regularizer = 0.01;
% 
% clusters = 30;
% repetitions = 10;
% covarianceBound = 1e-4;
% 
% [ results_vggs_100_d01_30_10 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);
% 
% clusters = 45;
% repetitions = 20;
% covarianceBound = 1e-6;
% 
% [ results_vggs_100_d01_45_20 ] = crossValidateD1(netType, regionSize, regularizer, ...
%     clusters, repetitions, covarianceBound, svmCost, kernelType);

