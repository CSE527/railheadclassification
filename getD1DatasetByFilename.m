function [ imdb ] = getD1DatasetByFilename( dataRoot )
%GETD1DATASET Returns an in-memory database of the D1 dataset.
%   The structure will be as follows:
% images - cell of structures, each containing filename, severity
%          and imageMean.
% sets - a 4-member cell containing structures, each containing:
%        train - a set of image indices to the training images
%        test - a set of image indices to the test images
%        trainingMean - the mean of the training images.\
% imageMean - the mean of all images.

    datafilePath = fullfile(dataRoot, 'D1Data.mat');

    if exist(datafilePath,'file')
        imdb = load(datafilePath);       
    else        
        imagePath = fullfile(dataRoot, 'KLD_TF/Images');
        imageFiles = dir(fullfile(imagePath, '*.bmp'));

        imdb = {};    
        numberOfImages = size(imageFiles,1);
        imdb.images = cell(1, numberOfImages);
        imdb.sets = cell(1, 4);

        % 4 fold cross validation set preparation
        % First Set
        train1Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Train1.txt')));   %read entire file into string
        train1parts = strtrim(regexp( train1Str, '(\r|\n)+', 'split'));  %split by each line    
        test1Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Test1.txt')));   %read entire file into string
        test1parts = strtrim(regexp( test1Str, '(\r|\n)+', 'split'));  %split by each line

        % Second Set
        train2Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Train2.txt')));   %read entire file into string
        train2parts = strtrim(regexp( train2Str, '(\r|\n)+', 'split'));  %split by each line    
        test2Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Test2.txt')));   %read entire file into string
        test2parts = strtrim(regexp( test2Str, '(\r|\n)+', 'split'));  %split by each line

        % Third Set
        train3Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Train3.txt')));   %read entire file into string
        train3parts = strtrim(regexp( train3Str, '(\r|\n)+', 'split'));  %split by each line    
        test3Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Test3.txt')));   %read entire file into string
        test3parts = strtrim(regexp( test3Str, '(\r|\n)+', 'split'));  %split by each line

        % Fourth Set
        train4Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Train4.txt')));   %read entire file into string
        train4parts = strtrim(regexp( train4Str, '(\r|\n)+', 'split'));  %split by each line    
        test4Str = strtrim(fileread(fullfile(dataRoot, 'Datasets/Test4.txt')));   %read entire file into string
        test4parts = strtrim(regexp( test4Str, '(\r|\n)+', 'split'));  %split by each line        

        set1train = zeros(1, numel(train1parts));
        set1test = zeros(1, numel(test1parts));
        set2train = zeros(1, numel(train2parts));
        set2test = zeros(1, numel(test2parts));
        set3train = zeros(1, numel(train3parts));
        set3test = zeros(1, numel(test3parts));
        set4train = zeros(1, numel(train4parts));
        set4test = zeros(1, numel(test4parts));

        trainIx1 = 1;
        testIx1 = 1;
        trainIx2 = 1;
        testIx2 = 1;
        trainIx3 = 1;
        testIx3 = 1;
        trainIx4 = 1;
        testIx4 = 1;

        sum = 0;
        
        for i = 1:numberOfImages
            imagename = imageFiles(i).name;
            severity = getSeverityFromFilename(imagename);
            image = imread(fullfile(imagePath, imagename));        

            sets = getSetLabelForFileNameFor4FoldCrossValidation(train1parts, test1parts, ...
                                                     train2parts, test2parts, ...
                                                     train3parts, test3parts, ...
                                                     train4parts, test4parts, ...
                                                     imagename);
            
            imdb.images{i}.filename = imagename;                                    
            %imdb.images{i}.data = im2single(image);
            image = im2single(image);            
            
            imdb.images{i}.imageMean = mean(image(:));
            imdb.images{i}.severity = severity;
            
            sum = sum + imdb.images{i}.imageMean;

            if (sets(1) == 1)
                set1train(trainIx1) = i;
                trainIx1 = trainIx1 + 1;
            else
                set1test(testIx1) = i;
                testIx1 = testIx1 + 1;
            end

            if (sets(2) == 1)
                set2train(trainIx2) = i;
                trainIx2 = trainIx2 + 1;
            else
                set2test(testIx2) = i;
                testIx2 = testIx2 + 1;
            end

            if (sets(3) == 1)
                set3train(trainIx3) = i;
                trainIx3 = trainIx3 + 1;
            else
                set3test(testIx3) = i;
                testIx3 = testIx3 + 1;
            end

            if (sets(4) == 1)
                set4train(trainIx4) = i;
                trainIx4 = trainIx4 + 1;
            else
                set4test(testIx4) = i;
                testIx4 = testIx4 + 1;
            end        

        end

        imdb.sets{1}.train = set1train;
        imdb.sets{1}.test = set1test;

        imdb.sets{2}.train = set2train;
        imdb.sets{2}.test = set2test;

        imdb.sets{3}.train = set3train;
        imdb.sets{3}.test = set3test;

        imdb.sets{4}.train = set4train;
        imdb.sets{4}.test = set4test;

        imdb.sets{1}.trainingMean = computeMean(imdb.images, imdb.sets{1}.train);
        imdb.sets{2}.trainingMean = computeMean(imdb.images, imdb.sets{2}.train);
        imdb.sets{3}.trainingMean = computeMean(imdb.images, imdb.sets{3}.train);
        imdb.sets{4}.trainingMean = computeMean(imdb.images, imdb.sets{4}.train);
        
        imdb.imageMean = sum / numberOfImages;

        save(datafilePath, '-struct', 'imdb', '-v7.3') ;
    end    

end


function [ imageMean ] = computeMean(images, indexes)
    sum = 0;
    count = size(indexes, 2);
    for i=1:count
        %sum = sum + mean(images{indexes(i)}.data(:));
        sum = sum + images{indexes(i)}.imageMean;
    end

    imageMean = sum / count;
end